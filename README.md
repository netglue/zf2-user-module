# Netglue ZF2 User/ACL Module

This is a ZF2 module that combines ACL and user management. It's in active development and not considered stable.
Rather than using other modules that achieve the same functionality, I decided to create my own so I could learn something!

You'll probably be better off looking at:

* [ZFCUser](https://github.com/ZF-Commons/ZfcUser)
* [ZFCUserDoctrineORM](https://github.com/ZF-Commons/ZfcUserDoctrineORM)
* [ZFCRbac](https://github.com/ZF-Commons/ZfcRbac)
* [BjyAuthorize](https://github.com/bjyoungblood/BjyAuthorize)

All of the above have been used to a lesser or greater extent in the development of this module.


## Docs

Find more docs in the `./docs` directory.

## To Do

* It would be good to emit events on logout. The only place I can see to do this is when storage is cleared.

* Perhaps implement a chain adapter like ZFC User for authentication - that way it would be much easier to work with other auth systems like Facebook and twitter and emit all conceiveable events from the top-level chain adapter thereby solving the logout event issue

* Login Accounting - write a listener that performs login accounting, then we could see stats for this perhaps?

* Public Registration - either registration is immediate, or requires confirmation of an email address, in which case we create a registration request, and only permit access when the email has been confirmed with a token

* Password Reset - Reset password with an emailed token

