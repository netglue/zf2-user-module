<?php

return array(
	
	'template_path_stack' => array(
		'ng_user' => __DIR__ . '/../view',
		
	),
	
	'template_map' => array(
		'error/403' => __DIR__ . '/../view/netglue-user/error/403.phtml',
	),

	'strategies' => array(
		'ViewJsonStrategy',
	),
	
);