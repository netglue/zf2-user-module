<?php
/**
 * Service Configuration
 */
return array(
	'factories' => array(
		'NetglueUser\Service\PasswordCrypt' => 'NetglueUser\Service\PasswordCryptServiceFactory',
		'NetglueUser\Validator\Password' => 'NetglueUser\Service\PasswordValidatorFactory',
		'NetglueUser\PasswordList' => 'NetglueUser\Service\PasswordListFactory',
		
		'NetglueUser\Options\UserOptions' => function($sm) {
			$config = $sm->get('config');
			$opt = isset($config['netglue_user']['user_options']) ? $config['netglue_user']['user_options'] : array();
			return new \NetglueUser\Options\UserOptions($opt);
		},
		
		'NetglueUser\Validator\Username' => function($sm) {
			$config = $sm->get('config');
			$opt = isset($config['netglue_user']['username_validation_options']) ? $config['netglue_user']['username_validation_options'] : array();
			return new \NetglueUser\Validator\Username($opt);
		},
		'NetglueUser\Repository\UserRepository' => function($sm) {
			$em = $sm->get('doctrine.entitymanager.orm_default');
			return $em->getRepository('NetglueUser\Entity\User');
		},
		'NetglueUser\Validator\NewUsername' => function($sm) {
			$instance = new \NetglueUser\Validator\NewUsername;
			$instance->setUserRepository($sm->get('NetglueUser\Repository\UserRepository'));
			return $instance;
		},
		'NetglueUser\Validator\NewEmail' => function($sm) {
			$instance = new \NetglueUser\Validator\NewEmail;
			$instance->setUserRepository($sm->get('NetglueUser\Repository\UserRepository'));
			return $instance;
		},
		// Auth storage needs to able to find users using the repo
		'NetglueUser\Authentication\Storage\Db' => function($sm) {
			$instance = new \NetglueUser\Authentication\Storage\Db;
			$instance->setUserRepository($sm->get('NetglueUser\Repository\UserRepository'));
			return $instance;
		},
		'NetglueUser\Authentication\Adapter\Db' => function($sm) {
			$instance = new \NetglueUser\Authentication\Adapter\Db;
			$instance->setUserRepository($sm->get('NetglueUser\Repository\UserRepository'));
			$instance->setPasswordCryptService($sm->get('NetglueUser\Service\PasswordCrypt'));
			return $instance;
		},
		'NetglueUser\Authentication\Service' => function($sm) {
			$service = new \Zend\Authentication\AuthenticationService(
				$sm->get('NetglueUser\Authentication\Storage\Db'),
				$sm->get('NetglueUser\Authentication\Adapter\Db')
			);
			return $service;
		},
		'NetglueUser\AclConfig' => function($sm) {
			$config = $sm->get('Config');
			return $config['netglue_user']['acl'];
		},
		'NetglueUser\Service\Acl' => function($sm) {
			$service = new \NetglueUser\Service\Acl;
			$service->setServiceLocator($sm);
			$config = $sm->get('NetglueUser\AclConfig');
			$service->setRoleConfig($config['roles']);
			$service->setRouteConfig($config['routes']);
			return $service;
		},
		'NetglueUser\Guard\Route' => function($sm) {
			$acl = $sm->get('NetglueUser\Service\Acl');
			$guard = new \NetglueUser\Guard\Route($acl);
			return $guard;
		},
		'NetglueUser\View\Strategy' => function($sm) {
			$config = $sm->get('NetglueUser\AclConfig');
			return $sm->get($config['error_strategy']);
		},
		'NetglueUser\View\Strategy\Unauthorized' => function($sm) {
			$strategy = new \NetglueUser\View\Strategy\Unauthorized;
			$config = $sm->get('NetglueUser\AclConfig');
			$strategy->setTemplate($config['error_template']);
			return $strategy;
		},
		'NetglueUser\View\Strategy\Redirect' => function($sm) {
			$strategy = new \NetglueUser\View\Strategy\Redirect;
			$config = $sm->get('NetglueUser\AclConfig');
			$strategy->setRedirectUri($config['redirect_uri']);
			$strategy->setRedirectRoute($config['redirect_route']);
			return $strategy;
		},
		'NetglueUser\View\Strategy\Composite' => function($sm) {
			$strategy = new \NetglueUser\View\Strategy\Composite;
			$strategy->setAuthenticationService($sm->get('NetglueUser\Authentication\Service'))
				->setRedirectStrategy($sm->get('NetglueUser\View\Strategy\Redirect'))
				->setUnauthStrategy($sm->get('NetglueUser\View\Strategy\Unauthorized'));
			return $strategy;
		},
	),
	
	'initializers' => array(
		// Inject object manager into anything that accepts it...
		function($instance, $sl) {
			if($instance instanceof \DoctrineModule\Persistence\ObjectManagerAwareInterface) {
				$instance->setObjectManager($sl->get('doctrine.entitymanager.orm_default'));
			}
		},
	),
	
	'invokables' => array(
		// Primary Service for dealing with users
		'NetglueUser\Service\User' => 'NetglueUser\Service\User',
		
		'NetglueUser\Authentication\LogListener' => 'NetglueUser\Authentication\LogListener',
	),
	
	'abstract_factories' => array(
		/**
		 * Abstract Factory for Generating Form Filters
		 */
		'NetglueUser\Service\InputFilterFactory',
	),
	
	'aliases' => array(
		// Alias the auth service so that the identity() controller plugin works
		'Zend\Authentication\AuthenticationService' => 'NetglueUser\Authentication\Service',
	),
);
