<?php

$factory = new \NetglueUser\Service\ControllerFactory;
$factory->setAliases(array(
	'ng_user_admin' => 'NetglueUser\Controller\AdminController',
	'ng_user_public' => 'NetglueUser\Controller\PublicController',
));

return array(
	'invokables' => array(
		
	),
	'abstract_factories' => array(
		'ng_user_abstract_controller_factory' => $factory,
	),
	'factories' => array(
	
	),
	'aliases' => array(
		
	),
);