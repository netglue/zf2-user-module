<?php

return array(
	'ng_user_admin' => array(
		'type' => 'Literal',
		'options' => array(
			'route' => '/admin/user',
			'defaults' => array(
				'controller' => 'ng_user_admin',
				'action' => 'index',
			),
		),
		'may_terminate' => true,
		'child_routes' => array(
			'create' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/create',
					'defaults' => array(
						'action' => 'create-user',
					),
				),
			),
			'edit' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/edit[/:id]',
					'defaults' => array(
						'action' => 'edit-user',
					),
					'constraints' => array(
						'id' => '[0-9]+',
					),
				),
			),
			'search_json' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/json[/:query]',
					'defaults' => array(
						'action' => 'json-search',
					),
				),
			),
		),
	),
	
	'ng_user_public' => array(
		'type' => 'Literal',
		'options' => array(
			'route' => '/account',
			'defaults' => array(
				'controller' => 'ng_user_public',
			),
		),
		'may_terminate' => true,
		'child_routes' => array(
			'login' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/login',
					'defaults' => array(
						'action' => 'login',
					),
				),
			),
			'logout' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/logout',
					'defaults' => array(
						'action' => 'logout',
					),
				),
			),
			'home' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/home',
					'defaults' => array(
						'action' => 'index',
					),
				),
			),
			'password' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/change-password',
					'defaults' => array(
						'action' => 'password',
					),
				),
			),
		),
	),
);