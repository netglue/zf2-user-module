<?php
/**
 * Configuration Specific To the ACL
 */
return array(
	
	/**
	 * When using the unauthorized strategy, we need to know
	 * which template to render
	 */
	'error_template' => 'error/403',
	
	/**
	 * When using the redirection strategy, the redirect uri, or route name
	 * must be set. To use the route name, the redirect uri must be null
	 */
	'redirect_uri' => NULL,
	'redirect_route' => 'ng_user_public/login',
	
	/**
	 * Name of the strategy to be pulled from the service manager and attached to the
	 * dispatch error
	 */
	
	/**
	 * The unauthorized strategy renders a 403 error page
	 */
	//'error_strategy' => 'NetglueUser\View\Strategy\Unauthorized',
	
	/**
	 * The redirect strategy redirects the user to a login page (Or somewhere else if configured)
	 */
	//'error_strategy' => 'NetglueUser\View\Strategy\Redirect',
	
	/**
	 * The composite uses both of the above.
	 * If there is no current identity, we are redirected to login
	 * If there is an identity without sufficient permission, the 403 error page is shown
	 */
	'error_strategy' => 'NetglueUser\View\Strategy\Composite',
	
	/**
	 * No multiple inheritance
	 */
	'roles' => array(
		array('name' => 'Guest', 'parent' => NULL),
		array('name' => 'User', 'parent' => 'Guest'),
		array('name' => 'Administrator', 'parent' => 'User'),
		array('name' => 'Root User', 'parent' => NULL),
	),
	
	/**
	 * Route Guard definitions are keyed so that they can be
	 * overridden, but they don't have to have named keys
	 *
	 */
	'routes' => array(
		'ng_user_public' => array(
			'route' => 'ng_user_public',
			'roles' => array('Guest'),
			'children' => array(
				'ng_user_public/index',
				'ng_user_public/login',
				'ng_user_public/logout',
			),
		),
		'ng_user_public/home' => array(
			'route' => 'ng_user_public/home',
			'roles' => array('User'),
			'children' => array(
				'ng_user_public/password',
			),
		),
		'ng_user_admin' => array(
			'route' => 'ng_user_admin',
			'roles' => array('Administrator'),
			'children' => array(
				'ng_user_admin/create',
				'ng_user_admin/edit',
				'ng_user_admin/search_json',
			),
		),
	),
	
);
