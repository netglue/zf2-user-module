<?php
/**
 * Base Configuration for the Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

/**
 * Route to redirect to on successful login
 * Only used if there was not a previously requested resource that should be
 * preferably redirected to
 */
$loginRedirectRoute = 'ng_user_public/home';

/**
 * Route to redirect to on logout
 */
$logoutRedirectRoute = 'ng_user_public/login';

/**
 * By Default, a simple array of disallowed passwords is configured
 * You can add more to the array
 * ['netglue_user']['password_list_options']['options']['data']
 */
$disallowedPasswords = array(
	'password',
	'PASSWORD',
	'Password',
	'qwertyuiop',
	'PA55W0RD',
	'pa55w0rd',
);

/**
 * Validation Options for the password validator
 * The password validator is provided as a simple way to enforce reasonable user passwords
 * Take a look at src/NetglueUser/Service/PasswordValidatorFactory and src/NetglueUser/Validator/Password
 */
$passwordValidationOptions = array(
	'minLength' => 8,               // Minimum Password Length
	'maxLength' => NULL,            // Maximum Password Length. NULL means no max length
	'requireNumbers' => true,       // Should passwords contain a number?
	'requireMixedCase' => true,     // Should passwords contain a mixture of lower and upper chrs?
	'requirePunctuation' => false,  // Should passwords contain at least 1 non-alpanumeric chr?
	'useWordList' => true,          // Should we prohibit password that match a pre-defined list?
);

/**
 * Settings control password hashing functions. The defaults should be fine and
 * use Bcrypt in the same way as ZfcUser et al
 */
$passwordOptions = array(
	'class' => 'bcrypt', // Don't use the other options, md5/sha1
	'salt' => NULL,      // If using md5/sha1 - Create a long random salt. Changing the salt will invalidate all password hashes stored.
	'cost' => 14,        // CPU cost setting for Bcrypt
);

/**
 * The DB Word list is not used by default so this is example config.
 * Look at the factory in src/NetglueUser/Service/PasswordListFactory
 * the 'dbService' option should be the name of service that returns a Zend DB Adapter
 * table name and column name should be self explanatory... The query run against the table
 * is a simple SELECT COUNT(*) AS num FROM table WHERE passColumn = 'password'
 */
$dbWordListOptions = array(
	'name' => 'db',
	'options' => array(
		'dbService' => 'Zend\Db\Adapter\AdapterServiceFactory', // Change this to whatever db adapter service name hooks up to the word list table
		'tableName' => 'ng_user_password_list',
		'columnName' => 'password',
	),
);

/**
 * By default, a simple array object is used to store a list of passwords we don't
 * allow. See above in $disallowedPasswords
 */
$arrayWordListOptions = array(
	'name' => 'array',
	'options' => array(
		'data' => $disallowedPasswords,
	),
);

/**
 * Username Validation.
 * Aside from not allowing duplicate usernames, you can impose restrictions on usernames
 */
$usernameValidationOptions = array(
	'minLength' => 3,
	'maxLength' => 100,
	
);

/**
 * Doctrine Configuration
 */
$doctrine = array(
	'driver' => array(
		'netglue_user_xml_driver' => array(
			'class' => 'Doctrine\ORM\Mapping\Driver\XmlDriver',
			'cache' => 'array',
			'paths' => array(
				__DIR__ . '/../data/mapping',
			),
		),
		'orm_default' => array(
			'drivers' => array(
				'NetglueUser\Entity' => 'netglue_user_xml_driver',
			),
		),
	),
);

/**
 * Routes
 */
$routes = include __DIR__ . '/routes.config.php';

/**
 * Controllers
 */
$controllers = include __DIR__ . '/controllers.config.php';

/**
 * View Manager
 */
$views = include __DIR__ . '/view.config.php';

/**
 * ACL Config
 */
$acl = include __DIR__ . '/acl.config.php';

/**
 * Return config keyed with 'netglue_user'
 */
return array(
	'netglue_user' => array(
		'password_options' => $passwordOptions,
		'password_validation_options' => $passwordValidationOptions,
		'password_list_options' => $arrayWordListOptions,
		'username_validation_options' => $usernameValidationOptions,
		
		'acl' => $acl,
		
		'login_redirect_route' => $loginRedirectRoute,
		'logout_redirect_route' => $logoutRedirectRoute,
	),
	'doctrine' => $doctrine,
	'router' => array(
		'routes' => $routes,
	),
	'controllers' => $controllers,
	'view_manager' => $views,
);
