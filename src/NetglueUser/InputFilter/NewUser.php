<?php

/**
 * An input filter specifically for filtering and validating data when new users are created
 */

namespace NetglueUser\InputFilter;

class NewUser extends AbstractUserFilter {
	
	public function init() {
		
		$this->add($this->getUsernameSpecification());
		$this->get('username')->getValidatorChain()->addValidator($this->getNewUsernameValidator());
		
		$this->add(array(
			'name' => 'email',
			'required' => true,
			'filters' => array(
				array('name' => 'StringToLower'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				array('name' => 'EmailAddress', 'break_chain_on_failure' => true),
				$this->getNewEmailValidator(),
			),
		));
		
		$this->add($this->getPasswordSpecification());
		
	}
	
}
