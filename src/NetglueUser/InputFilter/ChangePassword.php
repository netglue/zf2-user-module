<?php

/**
 * An input filter specifically for filtering and validating data when new users are created
 */

namespace NetglueUser\InputFilter;

class ChangePassword extends AbstractUserFilter {
	
	public function init() {
		
		$this->add(array(
			'name' => 'oldPassword',
			'required' => true,
		));
		
		$this->add($this->getPasswordSpecification());
		
		$this->add(array(
			'name' => 'verify',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'identical',
					'options' => array(
						'token' => 'password',
					),
				),
			),
		));
		
	}
	
}
