<?php
/**
 * Abstract filter class for validating user data
 */

namespace NetglueUser\InputFilter;
use Zend\InputFilter\InputFilter;
use Zend\Validator\ValidatorInterface as Validator;

use NetglueUser\Options\UserOptions;

abstract class AbstractUserFilter extends InputFilter {
	
	/**
	 * Password Validator
	 * @var Validator|NULL
	 */
	protected $passwordValidator;
	
	/**
	 * Username Validator
	 * @var Validator|NULL
	 */
	protected $usernameValidator;
	
	/**
	 * User Options
	 * @var UserOptions|NULL
	 */
	protected $userOptions;
	
	protected $newUsernameValidator;
	protected $newEmailValidator;
	/**
	 * Set Password Validator
	 * @param Validator $validator
	 * @return User $this
	 */
	public function setPasswordValidator(Validator $validator) {
		$this->passwordValidator = $validator;
		return $this;
	}
	
	/**
	 * Get Password Validator
	 * @return Validator|NULL
	 */
	public function getPasswordValidator() {
		return $this->passwordValidator;
	}
	
	/**
	 * Set Username Validator
	 * @param Validator $validator
	 * @return User $this
	 */
	public function setUsernameValidator(Validator $validator) {
		$this->usernameValidator = $validator;
		return $this;
	}
	
	/**
	 * Get Username Validator
	 * @return Validator|NULL
	 */
	public function getUsernameValidator() {
		return $this->usernameValidator;
	}
	
	/**
	 * Set New Username Validator
	 * @param Validator $validator
	 * @return User $this
	 */
	public function setNewUsernameValidator(Validator $validator) {
		$this->newUsernameValidator = $validator;
		return $this;
	}
	
	/**
	 * Get Username Validator
	 * @return Validator|NULL
	 */
	public function getNewUsernameValidator() {
		return $this->newUsernameValidator;
	}
	
	/**
	 * Set New Email Validator
	 * @param Validator $validator
	 * @return User $this
	 */
	public function setNewEmailValidator(Validator $validator) {
		$this->newEmailValidator = $validator;
		return $this;
	}
	
	/**
	 * Get Email Validator
	 * @return Validator|NULL
	 */
	public function getNewEmailValidator() {
		return $this->newEmailValidator;
	}
	
	/**
	 * Set User Options
	 * @param UserOptions $options
	 * @return AbstractUserFilter $this
	 */
	public function setUserOptions(UserOptions $options) {
		$this->userOptions = $options;
		return $this;
	}
	
	/**
	 * Return User Options
	 * @return UserOptions|NULL
	 */
	public function getUserOptions() {
		return $this->userOptions;
	}
	
	public function getUsernameSpecification() {
		return array(
			'name' => 'username',
			'required' => false,
			'filters' => array(
				array('name' => 'StringToLower'),
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				$this->getUsernameValidator(),
			),
		);
	}
	
	public function getPasswordSpecification() {
		return array(
			'name' => 'password',
			'required' => true,
			'filters' => array(
				array('name' => 'StringTrim'),
			),
			'validators' => array(
				$this->getPasswordValidator(),
			),
		);
	}
}
