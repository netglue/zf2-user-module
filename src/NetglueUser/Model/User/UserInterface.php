<?php

namespace NetglueUser\Model\User;

interface UserInterface {
	
	/**
	 * Return a unique identifier for the user
	 * @return int
	 */
	public function getId();
	
	/**
	 * Return the username
	 * @return string|NULL
	 */
	public function getUsername();
	
	/**
	 * Set the username
	 * @param string $username
	 * @return UserInterface $this
	 */
	public function setUsername($username);
	
	/**
	 * Return a hash of the current password
	 * @return string|NULL
	 */
	public function getHashedPassword();
	
	/**
	 * Deactivate the user account
	 * @return bool Whether deactivation was successful or not
	 */
	public function deactivate();
	
	/**
	 * Activate the user account
	 * @return bool
	 */
	public function activate();
	
	/**
	 * Whether the account is active or not
	 * @return bool
	 */
	public function isActive();
	
	/**
	 * Set the email address
	 * @param string $email
	 * @return UserInterface
	 */
	public function setEmail($email);
	
	/**
	 * Return email address
	 * @return string|NULL
	 */
	public function getEmail();
	
}