<?php

namespace NetglueUser\Model\User;

abstract class AbstractUser implements UserInterface {
	
	/**
	 * Id
	 * @var int|NULL
	 */
	protected $id;
	
	/**
	 * Username
	 * @var string|NULL
	 */
	protected $username;
	
	/**
	 * Email Address
	 * @var string|NULL
	 */
	protected $email;
	
	/**
	 * Hashed Password
	 * @var string|NULL
	 */
	protected $hashedPassword;
	
	/**
	 * Whether the account is active or not
	 * @var bool
	 */
	protected $isActive;
	
	/**
	 * Return a unique identifier for the user
	 * @return int|NULL
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Return the username
	 * @return string|NULL
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * Set the username
	 * @param string $username
	 * @return UserInterface $this
	 */
	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}
	
	/**
	 * Set hashed password
	 * @param string $password
	 * @return UserInterface $this
	 */
	protected function setHashedPassword($password) {
		$this->hashedPassword = $password;
		return $this;
	}
	
	/**
	 * Return a hash of the current password
	 * @return string|NULL
	 */
	public function getHashedPassword() {
		return $this->hashedPassword;
	}
	
	/**
	 * Deactivate the user account
	 * @return bool Whether deactivation was successful or not
	 */
	public function deactivate() {
		$this->isActive = false;
		return true;
	}
	
	/**
	 * Activate the user account
	 * @return bool
	 */
	public function activate() {
		$this->isActive = true;
		return $this;
	}
	
	/**
	 * Whether the account is active or not
	 * @return bool
	 */
	public function isActive() {
		return true === $this->isActive;
	}
	
	/**
	 * Set the email address
	 * @param string $email
	 * @return UserInterface
	 */
	public function setEmail($email) {
		$this->email = trim(strtolower($email));
		$this->email = empty($this->email) ? NULL : $this->email;
		return $this;
	}
	
	/**
	 * Return email address
	 * @return string|NULL
	 */
	public function getEmail() {
		return $this->email;
	}
	
}