<?php
namespace NetglueUser;

/**
 * Autoloader
 */
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Controller Plugin Provider
 */
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;

/**
 * View Helper Provider
 */
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

/**
 * Form Element Provider
 */
use Zend\ModuleManager\Feature\FormElementProviderInterface;

/**
 * Bootstrap Listener
 */
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface as Event;

class Module implements
	AutoloaderProviderInterface,
	ServiceProviderInterface,
	ConfigProviderInterface,
	BootstrapListenerInterface,
	ControllerPluginProviderInterface,
	FormElementProviderInterface,
	ViewHelperProviderInterface {
	
	
	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 */
	public function getAutoloaderConfig() {
    return array(
			AutoloaderFactory::STANDARD_AUTOLOADER => array(
				StandardAutoloader::LOAD_NS => array(
					__NAMESPACE__ => __DIR__,
				),
			),
		);
	}
	
	/**
	 * Include/Return module configuration
	 * @return array
	 * @implements ConfigProviderInterface
	 */
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}
	
	/**
	 * Return Service Config
	 * @return array
	 * @implements ServiceProviderInterface
	 */
	public function getServiceConfig() {
		return include __DIR__ . '/../../config/services.config.php';
	}
	
	/**
	 * Return controller plugin config
	 * @return array
	 */
	public function getControllerPluginConfig() {
		return array(
			'factories' => array(
				
			),
			'aliases' => array(
				'authSessionRedirect' => 'NetglueUser\Controller\Plugin\AuthSessionRedirect',
			),
			'invokables' => array(
				'NetglueUser\Controller\Plugin\AuthSessionRedirect' => 'NetglueUser\Controller\Plugin\AuthSessionRedirect',
			),
		);
	}
	
	public function getFormElementConfig() {
		return array(
			'factories' => array(
				'NetglueUserRole' => function($sm) {
					$sl = $sm->getServiceLocator();
					$acl = $sl->get('NetglueUser\Service\Acl');
					$element = new \NetglueUser\Form\Element\Role;
					$element->setRoles($acl->getRoles());
					return $element;
				},
			),
			'aliases' => array(
			//	'NetglueUserRole' => 'NetglueUser\Form\Element\Role',
			),
		);
	}
	
	public function getViewHelperConfig() {
		return array(
			'invokables' => array(
				'bsFormRow' => 'Netglue\Form\View\Helper\TwitterBootstrapFormRow',
			),
		);
	}
	
	/**
	 * MVC Bootstrap Event
	 *
	 * @param Event $e
	 * @return void
	 * @implements BootstrapListenerInterface
	 */
	public function onBootstrap(Event $e) {
		$app = $e->getApplication();
		$sm = $app->getServiceManager();
		$guard = $sm->get('NetglueUser\Guard\Route');
		$app->getEventManager()->attach($guard);
		$app->getEventManager()->attach($sm->get('NetglueUser\View\Strategy'));
		
		// This should only be enabled by choice in config
		$logger = $sm->get('NetglueUser\Authentication\LogListener');
		$app->getEventManager()->attach($logger);
	}
}
