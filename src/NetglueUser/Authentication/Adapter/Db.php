<?php
/**
 * Basic authadapter that uses the doctrine repo for looking up users
 *
 * It is expected that client code will set either the username or the email address
 * using $adapter->setIdentity($usernameOrEmail) as this is what is sent to the
 * user repository to locate matching user entities
 */


namespace NetglueUser\Authentication\Adapter;

/**
 * Abstract Adapter provides set/get credential/identity
 */
use Zend\Authentication\Adapter\AbstractAdapter as BaseAdapter;

/**
 * Password crypt service interface
 */
use NetglueUser\Service\PasswordCryptServiceInterface;

/**
 * Results returned from authenticate()
 */
use Zend\Authentication\Result;

/**
 * Exceptions that all implement \Zend\Authentication\Exception\ExceptionInterface
 */
use NetglueUser\Authentication\Exception;

/**
 * User Repo used for locating user objects
 */
use NetglueUser\Repository\UserRepository as UserRepo;

/**
 * Event Manager for trigerring events
 */
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class Db extends BaseAdapter implements EventManagerAwareInterface {
	
	const EVENT_IDENTITY_NOT_FOUND = 'errorIdentityNotFound';
	const EVENT_IDENTITY_AMBIGUOUS = 'errorIdentityAmbiguous';
	const EVENT_IDENTITY_DISABLED = 'errorIdentityDisabled';
	const EVENT_CREDENTIAL_INVALID = 'errorCredentialInvalid';
	const EVENT_AUTHENTICATE = 'authenticateSuccess';
	
	/**
	 * Doctrine Repository for locating Users
	 * @var UserRepo|NULL
	 */
	protected $repo;
	
	/**
	 * Password Crypt Service
	 * We need to be given one of these so that we know how to match up passwords
	 * @var PasswordCryptServiceInterface|NULL
	 */
	protected $cryptService;
	
	/**
	 * Event Manager
	 * @var EventManagerInterface|NULL
	 */
	protected $eventManager;
	
	/**
	 * Set Repository for locating User
	 * @param UserRepo $repo
	 * @return Db $this
	 */
	public function setUserRepository(UserRepo $repo) {
		$this->repo = $repo;
		return $this;
	}
	
	/**
	 * Return User Repository
	 * @return UserRepo|NULL
	 */
	public function getUserRepository() {
		return $this->repo;
	}
	
	/**
	 * Set the password crypt service we're using for treating passwords
	 * @param PasswordCryptServiceInterface $service
	 * @return Db $this
	 */
	public function setPasswordCryptService(PasswordCryptServiceInterface $service) {
		$this->cryptService = $service;
		return $this;
	}
	
	/**
	 * Return password crypt service
	 * @return PasswordCryptServiceInterface|NULL
	 */
	public function getPasswordCryptService() {
		return $this->cryptService;
	}
	
	/**
	 * Primary authentication method
	 * @return Result
	 * @throws Exception\ExceptionInterface
	 */
	public function authenticate() {
		$this->requireNonEmptyCredentials();
		$this->requireDependencies();
		$users = $this->getIdentityMatches();
		
		if(count($users) === 0) {
			$result = new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->getIdentity(), array('No user account could be located with the username or email address provided'));
			$this->eventManager->trigger(static::EVENT_IDENTITY_NOT_FOUND, $this, array(
				'result' => $result,
				'identity' => $this->getIdentity(),
				'credential' => $this->getCredential(),
			));
			return $result;
		}
		
		if(count($users) > 1) {
			$result = new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, $this->getIdentity(), array('Multiple identities were located with the username or email address provided'));
			$this->eventManager->trigger(static::EVENT_IDENTITY_AMBIGUOUS, $this, array(
				'result' => $result,
				'identity' => $this->getIdentity(),
				'credential' => $this->getCredential(),
				'users' => $users,
			));
			return $result;
		}
		
		$user = current($users);
		
		if(!$user->isActive()) {
			$result = new Result(Result::FAILURE_UNCATEGORIZED, $this->getIdentity(), array("The account for {$this->getIdentity()} is not active"));
			$this->eventManager->trigger(static::EVENT_IDENTITY_DISABLED, $this, array(
				'result' => $result,
				'identity' => $this->getIdentity(),
				'credential' => $this->getCredential(),
				'user' => $user,
			));
			return $result;
		}
		
		if($this->getPasswordCryptService()->verify($this->getCredential(), $user->getHashedPassword()) !== true) {
			$result = new Result(Result::FAILURE_CREDENTIAL_INVALID, $this->getIdentity(), array('Invalid password provided'));
			$this->eventManager->trigger(static::EVENT_CREDENTIAL_INVALID, $this, array(
				'result' => $result,
				'identity' => $this->getIdentity(),
				'credential' => $this->getCredential(),
				'user' => $user,
			));
			return $result;
		}
		
		
		
		$result = new Result(Result::SUCCESS, $this->getIdentity());
		$this->eventManager->trigger(static::EVENT_AUTHENTICATE, $this, array(
			'result' => $result,
			'identity' => $this->getIdentity(),
			'credential' => $this->getCredential(),
			'user' => $user,
		));
		return $result;
	}
	
	/**
	 * Return an array of user entites that match the provided identity
	 * @return array
	 */
	protected function getIdentityMatches() {
		return $this->getUserRepository()->findByIdentity($this->getIdentity());
	}
	
	/**
	 * Throw an exception if password or username have not been set
	 * @throws Exception\RuntimeException
	 */
	protected function requireNonEmptyCredentials() {
		$u = $this->getIdentity();
		$p = $this->getCredential();
		if(empty($u)) {
			throw new Exception\RuntimeException('No identity has been provided prior to authentication');
		}
		if(empty($p)) {
			throw new Exception\RuntimeException('No credential has been provided prior to authentication');
		}
	}
	
	/**
	 * Throw an exception if we're missing any dependencies
	 * @throws Exception\ExceptionInterface
	 */
	protected function requireDependencies() {
		if(!$this->getPasswordCryptService() instanceof PasswordCryptServiceInterface) {
			throw new Exception\RuntimeException('No password crypt service has been provided to the authentication adapter');
		}
		if(!$this->getUserRepository() instanceof UserRepo) {
			throw new Exception\RuntimeException('No user repository has been provided to the authentication adapter');
		}
	}
	
	/**
	 * Set Event Manager
	 * @param EventManagerInterface $events
	 * @return EventManagerAwareInterface $this
	 */
	public function setEventManager(EventManagerInterface $events) {
		$this->eventManager = $events;
		 $events->setIdentifiers(array(
			__CLASS__,
			get_class($this),
		));
		return $this;
	}
	
	/**
	 * Return event manager
	 * @return EventManagerInterface|NULL
	 */
	public function getEventManager() {
		return $this->eventManager;
	}
	 
}