<?php

namespace NetglueUser\Authentication\Exception;

use Zend\Authentication\Exception\ExceptionInterface as AuthException;

interface ExceptionInterface extends AuthException {

}