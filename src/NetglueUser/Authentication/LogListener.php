<?php
namespace NetglueUser\Authentication;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;

/**
 * Listening to event emitted from the DB Adapter
 */
use NetglueUser\Authentication\Adapter\Db as AuthAdapter;

use Zend\Log\LoggerAwareInterface;
use Zend\Log\LoggerInterface;

class LogListener implements ListenerAggregateInterface, LoggerAwareInterface {
	
	protected $listeners = array();
	
	protected $logger;
	
	/**
	 * Attach to events
	 */
	public function attach(EventManagerInterface $eventManager) {
		$events = $eventManager->getSharedManager();
		$this->listeners[] = $events->attach('NetglueUser\Authentication\Adapter\Db', '*', array($this, 'logEvent'));
	}
	
	public function detach(EventManagerInterface $events) {
		foreach($this->listeners as $index => $listener) {
			if($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}
	
	
	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
		return $this;
	}
	
	public function getLogger() {
		return $this->logger;
	}
	
	public function hasLogger() {
		return NULL !== $this->logger;
	}
	
	public function logEvent(EventInterface $event) {
		if(!$this->hasLogger()) {
			return;
		}
		
		$name = $event->getName();
		$id = $event->getParam('identity');
		$extra = array(
			'identity' => $event->getParam('identity'),
		);
		switch($name) {
			default:
				return;
				break;
			
			case AuthAdapter::EVENT_IDENTITY_NOT_FOUND:
				$this->logger->debug("Attempt to login with an invalid identity: {$id}", $extra);
				break;
			
			case AuthAdapter::EVENT_IDENTITY_AMBIGUOUS:
				$users = $event->getParam('users');
				$count = count($users);
				$ids = array();
				foreach($users as $u) {
					$ids[] = $u->getId();
				}
				$ids = implode(', ', $ids);
				$this->logger->err("Ambiguous identity returned for '{$id}': {$count} users found with conflicting identities [{$ids}]", $extra);
				break;
			
			case AuthAdapter::EVENT_IDENTITY_DISABLED:
				$extra['user_id'] = $event->getParam('user')->getId();
				$this->logger->info("The account for {$id} is inactive. Authentication prohibited", $extra);
				break;
			
			case AuthAdapter::EVENT_CREDENTIAL_INVALID:
				$extra['user_id'] = $event->getParam('user')->getId();
				$this->logger->info("Invalid password supplied for {$id}", $extra);
				break;
				
			case AuthAdapter::EVENT_AUTHENTICATE:
				$extra['user_id'] = $event->getParam('user')->getId();
				$this->logger->info("{$id} has successfully logged in", $extra);
				break;
		}
		
	}
}