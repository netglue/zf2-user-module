<?php
/**
 * Auth storage adapter that returns the current identity as hydrated User Entity/Object
 * rather than a string held in the session.
 * Uses the standard session storage to retreive the current username and reads user information from the database
 */

namespace NetglueUser\Authentication\Storage;

use Zend\Authentication\Storage\StorageInterface;
use Zend\Authentication\Storage;

use NetglueUser\Repository\UserRepository as UserRepo;

/**
 * Namespaced exceptions that implement the Zend Auth Exception Interface
 */
use NetglueUser\Authentication\Exception;

class Db implements StorageInterface {
	
	/**
	 * Doctrine Repository for locating Users
	 * @var UserRepo|NULL
	 */
	protected $repo;
	
	/**
	 * Session storage where we find the username/identity
	 * @var StorageInterface|NULL
	 */
	protected $storage;
	
	/**
	 * The current identity
	 * @var \NetglueUser\Entity\User|NULL
	 */
	protected $identity;
	
	/**
	 * Set Repository for locating User
	 * @param UserRepo $repo
	 * @return Db $this
	 */
	public function setUserRepository(UserRepo $repo) {
		$this->repo = $repo;
		return $this;
	}
	
	/**
	 * Return User Repository
	 * @return UserRepo|NULL
	 */
	public function getUserRepository() {
		return $this->repo;
	}
	
	/**
	 * Return Session Storage by default
	 * @return StorageInterface
	 */
	public function getStorage() {
		if(!$this->storage instanceof StorageInterface) {
			$this->setStorage(new Storage\Session);
		}
		return $this->storage;
	}
	
	/**
	 * Set underlying storage mechanism
	 * @param StorageInterface $storage
	 * @return Db $this
	 */
	public function setStorage(StorageInterface $storage) {
		$this->storage = $storage;
		return $this;
	}
	
	/**
	 * Whether the underlying storage is empty or not, i.e. the session
	 * @see \Zend\Authentication\Storage\Session::isEmpty()
	 * @see \Zend\Authentication\Storage\StorageInterface::isEmpty()
	 * @return bool
	 */
	public function isEmpty() {
		return $this->getStorage()->isEmpty();
	}
	
	/**
	 * Write $contents to storage
	 * @param mixed $contents - In practice, this would be a string username
	 * @return void
	 */
	public function write($contents) {
		$this->identity = NULL;
		$this->getStorage()->write($contents);
	}
	
	/**
	 * Clear identity from storage
	 * @return void
	 */
	public function clear() {
		$this->identity = NULL;
		$this->getStorage()->clear();
	}
	
	/**
	 * Read/Return identity from storage
	 * @return \NetglueUser\Entity\User|NULL
	 */
	public function read() {
		if(NULL !== $this->identity) {
			return $this->identity;
		}
		
		$id = $this->getStorage()->read();
		if(empty($id)) {
			return NULL;
		}
		
		if(is_numeric($id)) {
			if(false !== ($user = $this->getUserRepository()->findById($id))) {
				$this->identity = current($users);
				return $this->identity;
			}
			return NULL;
		}
		
		if(is_string($id)) {
			$users = $this->getUserRepository()->findByIdentity($id);
			if(count($users) > 1) {
				throw new Exception\MultipleMatchException(sprintf('%d identity matches were found for %s', count($users), $id));
			}
			if(count($users)) {
				$this->identity = current($users);
				return $this->identity;
			}
			return NULL;
		}
		
		$value = is_scalar($id) ? $id : (is_object($id) ? get_class($id) : gettype($id));
		throw new Exception\UnexpectedValueException(sprintf('Unexpected identity returned by session storage. Expected a string or a number, received %s', $value));
		
		return $this->identity; // NULL
	}
	
}