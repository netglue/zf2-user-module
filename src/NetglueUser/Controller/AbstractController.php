<?php

/**
 * Controller with some basic logging functionality and service access
 */

namespace NetglueUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Logging
 */
use Zend\Log\LoggerAwareInterface;
use Zend\Log\LoggerInterface;

use NetglueUser\Service\User as UserService;

abstract class AbstractController extends AbstractActionController {
	
	/**
	 * Logger
	 * @var LoggerInterface|NULL
	 */
	protected $logger;
	
	/**
	 * User Service
	 * @var UserService|NULL
	 */
	protected $userService;
	
	/**
	 * Set Logger
	 * @param LoggerInterface $logger
	 * @return LoggerAwareInterface
	 */
	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
		return $this;
	}
	
	/**
	 * Return logger
	 * @return LoggerInterface|NULL
	 */
	public function getLogger() {
		return $this->logger;
	}
	
	/**
	 * Whether we have a logger
	 * @return bool
	 */
	public function hasLogger() {
		return $this->logger instanceof LoggerInterface;
	}
	
	/**
	 * Set User Service
	 * @param UserService $service
	 * @return AbstractController $this
	 */
	public function setUserService(UserService $service) {
		$this->userService = $service;
		return $this;
	}
	
	/**
	 * Get user service
	 * @return UserService|NULL
	 */
	public function getUserService() {
		return $this->userService;
	}
	
}