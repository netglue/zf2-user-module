<?php

/**
 * Controller intended for 'admin' users for the purpose of CRUD operations on users
 */

namespace NetglueUser\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Zend\Stdlib\ResponseInterface as Response;

use Zend\Form\FormInterface;

class AdminController extends AbstractController {
	
	const ROUTE_CREATE = 'ng_user_admin/create';
	const ROUTE_EDIT = 'ng_user_admin/edit';
	const ROUTE_INDEX = 'ng_user_admin';
	
	protected $acceptCriteria = array(
		'Zend\View\Model\JsonModel' => array(
			'application/json',
		),
	);
	
	public function indexAction() {
		return new ViewModel;
	}
	
	
	/**
	 * Create a User
	 * @return ViewModel
	 */
	public function createUserAction() {
		
		$prg = $this->prg(static::ROUTE_CREATE);
		
		if($prg instanceof Response) {
			return $prg;
		}
		
		$view = new ViewModel;
		
		$form = $view->form = $this->getUserService()->getCreateUserForm();
		
		if(false === $prg) {
			// Render Form
			return $view;
		}
		
		$post = $prg;
		
		$form->setData($post);
		
		if(!$form->isValid()) {
			// Validation Failed
			return $view;
		}
		
		// Save User
		$user = $this->getUserService()->createUser($form->getData());
		if(false === $user) {
			// Validation has subsequently failed
			return $view;
		}
		$this->flashMessenger()->addSuccessMessage(sprintf('User account #%d created', $user->getId()));
		return $this->redirect()->toRoute(static::ROUTE_INDEX);
	}
	
	/**
	 * Modify an existing user
	 * @return ViewModel
	 */
	public function editUserAction() {
		// Locate User
		$params = $this->params()->fromRoute();
		$id = $params['id'];
		if(empty($id) || !is_numeric($id)) {
			$this->flashMessenger()->addErrorMessage('No user account identifier was provided');
			return $this->redirect()->toRoute(static::ROUTE_INDEX);
		}
		
		$service = $this->getUserService();
		$repo = $service->getUserRepository();
		$user = $repo->findById($id);
		if(false === $user) {
			$msg = 'No user account could be found with id %d';
			$this->flashMessenger()->addErrorMessage(sprintf($msg, $id));
			return $this->redirect()->toRoute(static::ROUTE_INDEX);
		}
		
		$prg = $this->prg($this->getRequest()->getUri(), true);
		
		if($prg instanceof Response) {
			return $prg;
		}
		
		$view = new ViewModel;
		$view->user = $user;
		$form = $view->form = $this->getUserService()->getEditUserForm();
		$form->bind($user);
		
		if(false === $prg) {
			// Render Form
			return $view;
		}
		
		$post = $prg;
		
		$form->setData($post);
		
		if(!$form->isValid()) {
			// Validation Failed
			return $view;
		}
		
		// Save User
		$result = $this->getUserService()->updateUser($user, $form->getData(FormInterface::VALUES_AS_ARRAY));
		if(false === $result) {
			// Validation has subsequently failed
			return $view;
		}
		$this->flashMessenger()->addSuccessMessage(sprintf('Changes to user account #%d have been saved', $user->getId()));
		return $this->redirect()->toRoute(static::ROUTE_INDEX);
	}
	
	/**
	 * Given query, return Json encoded matches
	 * @return JsonModel
	 */
	public function jsonSearchAction() {
		$view = new JsonModel;
		$view->results = array();
		$view->count = 0;
		
		$query = $this->params()->fromRoute('query', NULL);
		$view->query = $query;
		
		if(empty($query)) {
			return $view;
		}
		
		$service = $this->getUserService();
		$repo = $service->getUserRepository();
		$results = array();
		foreach($repo->searchIdentity($query) as $user) {
			$results[] = $user->toJsonArray();
		}
		$view->results = $results;
		$view->count = count($view->results);
		return $view;
	}
	
}
