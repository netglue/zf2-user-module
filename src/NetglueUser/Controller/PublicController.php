<?php
/**
 * Controller intended for 'public' usage - ie login, registration, password reminder etc
 */

namespace NetglueUser\Controller;

use Zend\View\Model\ViewModel;

use Zend\Stdlib\ResponseInterface as Response;

use Zend\Form\FormInterface;

class PublicController extends AbstractController {
	
	const ROUTE_LOGIN = 'ng_user_public/login';
	const ROUTE_CHANGE_PASS = 'ng_user_public/password';
	const ROUTE_HOME = 'ng_user_public/home';
	
	
	
	/**
	 * Logged in user home page
	 * @return ViewModel
	 */
	public function indexAction() {
		if(!$this->identity()) {
			return $this->redirect()->toRoute(static::ROUTE_LOGIN);
		}
		$view = new ViewModel;
		$view->user = $this->identity();
		return $view;
	}
	
	/**
	 * Present a login form, or authenticate using posted data
	 * @return ViewModel|void
	 * @todo Easy way of specifying a redirection parameter
	 */
	public function loginAction() {
		
		if($this->identity()) {
			// Already logged in...
			return $this->redirect()->toRoute(static::ROUTE_HOME);
		}
		
		$prg = $this->prg(static::ROUTE_LOGIN);
		
		if($prg instanceof Response) {
			return $prg;
		}
		
		$view = new ViewModel;
		$form = $view->form = $this->getUserService()->getLoginForm();
		
		if(false === $prg) {
			// Render Form
			return $view;
		}
		
		$post = $prg;
		$form->setData($post);
		
		if(!$form->isValid()) {
			// Validation Failed
			return $view;
		}
		
		$service = $this->getAuthService();
		$adapter = $service->getAdapter();
		$data = $form->getData();
		$adapter->setIdentity($data['identity']);
		$adapter->setCredential($data['password']);
		$result = $service->authenticate();
		
		if(!$result->isValid()) {
			return $view;
		}
		/**
		 * If we have an original destination stored in the session
		 * redirect there, otherwise redirect to the home page
		 */
		$helper = $this->authSessionRedirect();
		if($helper->hasRedirect()) {
			$resp = $this->redirect()->toUrl($helper->getRedirectUri());
			// Clear the stored redirect so that subsequent logins don't redirect to the same location
			$helper->clear();
			return $resp;
		}
		return $this->redirect()->toRoute($this->getLoginRedirectRoute());
	}
	
	/**
	 * Logout...
	 * @return void
	 */
	public function logoutAction() {
		$service = $this->getAuthService();
		$service->clearIdentity();
		$this->redirect()->toRoute($this->getLogoutRedirectRoute());
	}
	
	/**
	 * Change password
	 * @return ViewModel
	 */
	public function passwordAction() {
		if(!$this->identity()) {
			// Surely this is not needed...
			return $this->redirect()->toRoute(static::ROUTE_LOGIN);
		}
		
		$prg = $this->prg(static::ROUTE_CHANGE_PASS);
		
		if($prg instanceof Response) {
			return $prg;
		}
		
		$view = new ViewModel;
		$user = $view->user = $this->identity();
		$form = $view->form = $this->getUserService()->getChangePasswordForm();
		
		if(false === $prg) {
			// Render Form
			return $view;
		}
		
		$post = $prg;
		
		$form->setData($post);
		
		if(!$form->isValid()) {
			// Validation Failed
			return $view;
		}
		
		// Save Password
		$result = $this->getUserService()->updatePassword($form);
		if(false === $result) {
			// Validation has subsequently failed
			return $view;
		}
		$this->flashMessenger()->addSuccessMessage('You have successfully changed your password');
		return $this->redirect()->toRoute(static::ROUTE_HOME);
	}
	
	/**
	 * Return Authentication Service
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthService() {
		return $this->getUserService()->getAuthenticationService();
	}
	
	public function getLoginRedirectRoute() {
		$conf = $this->getServiceLocator()->get('Config');
		$route = isset($conf['netglue_user']['login_redirect_route']) ? $conf['netglue_user']['login_redirect_route'] : self::ROUTE_HOME;
		return $route;
	}
	
	public function getLogoutRedirectRoute() {
		$conf = $this->getServiceLocator()->get('Config');
		$route = isset($conf['netglue_user']['logout_redirect_route']) ? $conf['netglue_user']['logout_redirect_route'] : self::ROUTE_LOGIN;
		return $route;
	}
	
}