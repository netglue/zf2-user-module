<?php
/**
 * A controller plugin that can be used to help redirect a successfully authenticated
 * user to the originally requested resource
 */
namespace NetglueUser\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

use NetglueUser\Session\RedirectContainer;


class AuthSessionRedirect extends AbstractPlugin {
	
	/**
	 * Session Container Instance
	 * @var RedirectContainer|NULL
	 */
	protected $session;
	
	/**
	 * Invoke just returns self so we can chain to other methods
	 * @return self
	 */
	public function __invoke() {
		return $this;
	}
	
	/**
	 * Return session container that holds redirect information
	 * @return RedirectContainer
	 */
	public function getSession() {
		if(!$this->session instanceof RedirectContainer) {
			$this->session = new RedirectContainer;
		}
		return $this->session;
	}
	
	/**
	 * Whether a redirect location of any sort is stored in the session
	 * @return bool
	 */
	public function hasRedirect() {
		return $this->getSession()->hasRedirect();
	}
	
	/**
	 * Return the redirect uri
	 * @return string
	 */
	public function getRedirectUri() {
		if(!$this->hasRedirect()) {
			return false;
		}
		if($this->getSession()->isUri()) {
			return $this->getSession()->getRedirectUri();
		}
		$route = $this->getSession()->getRedirectRoute();
		$controller = $this->getController();
		if(!$controller || !method_exists($controller, 'plugin')) {
			throw new \DomainException('Redirect plugin requires a controller that defines the plugin() method');
		}
		$urlPlugin = $controller->plugin('url');
		return $urlPlugin->fromRoute($route);
	}
	
	/**
	 * Clear any stored redirection information
	 * @return self
	 */
	public function clear() {
		$this->getSession()->clear();
		return $this;
	}
}