<?php

namespace NetglueUser\Options;

use Zend\StdLib\AbstractOptions;

class UserOptions extends AbstractOptions {
	
	protected $permitUsernames = true;
	
	protected $requireEmailVerificationBeforeLogin = true;
	
	protected $requireEmailVerification = true;
	
	/**
	 * Whether Usernames are allowed
	 * @param bool $flag
	 * @return UserOptions $this
	 */
	public function setPermitUsernames($flag) {
		$this->permitUsernames = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether Usernames are permitted
	 * @return bool
	 */
	public function getPermitUsernames() {
		return $this->permitUsernames;
	}
	
	/**
	 * Whether new user accounts require email verification before use
	 * @param bool
	 * @return UserOptions $this
	 */
	public function setRequireEmailVerificationBeforeLogin($flag) {
		$this->requireEmailVerificationBeforeLogin = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether email verification is required before account is activated
	 * @return bool
	 */
	public function getRequireEmailVerificationBeforeLogin() {
		return $this->requireEmailVerificationBeforeLogin;
	}
	
	/**
	 * Set Whether email verification is required at all
	 * @param bool $flag
	 * @return UserOptions $this
	 */
	public function setRequireEmailVerification($flag) {
		$this->requireEmailVerification = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether email verification is required
	 * @return bool
	 */
	public function getRequireEmailVerification() {
		return $this->requireEmailVerification;
	}
	
}