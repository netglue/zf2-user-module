<?php
/**
 * A factory to create a validator chain configured with password validation options
 *
 */

namespace NetglueUser\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use NetglueUser\Validator\Password as PasswordValidator;
use NetglueUser\PasswordList\PasswordListInterface;
use NetglueUser\PasswordList\ArrayWordList;

class PasswordValidatorFactory implements FactoryInterface {
	
	/**
	 * Return the configured feed
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return PasswordCryptServiceInterface
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('config');
		$options = isset($config['netglue_user']['password_validation_options']) ? $config['netglue_user']['password_validation_options'] : NULL;
		
		if(isset($options['useWordList']) && true === $options['useWordList']) {
			$list = $serviceLocator->get('NetglueUser\PasswordList');
			$options['wordList'] = $list;
		}
		$v = new PasswordValidator($options);
		return $v;
	}
	
}