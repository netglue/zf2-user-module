<?php
namespace NetglueUser\Service;

interface PasswordCryptServiceInterface {
	
	/**
	 * Encrypt a string
	 * @param string $string
	 * @return string
	 */
	public function encrypt($string);
	
	/**
	 * Verify a clear text string against an encrypted string
	 * @param string $clearText
	 * @param string $encrypted
	 * @return bool
	 */
	public function verify($clearText, $encrypted);
	
}