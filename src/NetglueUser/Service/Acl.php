<?php

/**
 * Acl Service - essentially a helper to populate the underlying Zend Acl instance
 */

namespace NetglueUser\Service;

use Zend\Permissions\Acl\Acl as ZendAcl;

use Zend\ServiceManager\ServiceLocatorInterface;

use NetglueUser\Guard\Route as RouteGuard;

class Acl {
	
	protected $acl;
	
	protected $serviceLocator;
	
	protected $defaultRoleId = 'Guest';
	
	protected $inited = false;
	
	public function __construct() {
		$this->acl = new ZendAcl;
	}
	
	protected function init() {
		if($this->inited) {
			return;
		}
		$this->inited = true;
		
	}
	
	public function setServiceLocator(ServiceLocatorInterface $services) {
		$this->serviceLocator = $services;
		return $this;
	}
	
	public function getServiceLocator() {
		return $this->serviceLocator;
	}
	
	public function getIdentity() {
		$authService = $this->getServiceLocator()->get('NetglueUser\Authentication\Service');
		if($authService->hasIdentity()) {
			return $authService->getIdentity();
		}
		return false;
	}
	
	/**
	 * Return role for the current authenticated identity
	 * @return string
	 */
	public function getCurrentRoleId() {
		if($user = $this->getIdentity()) {
			$roleId = $user->getRole();
			if(!empty($roleId) && $this->acl->hasRole($roleId)) {
				return $roleId;
			}
		}
		return $this->getDefaultRoleId();
	}
	
	public function getDefaultRoleId() {
		return $this->defaultRoleId;
	}
	
	public function getRoles() {
		return $this->acl->getRoles();
	}
	
	/**
	 * This is named so that setRoles() addRoles() can be added later
	 * For now this is a simple way to get role config into the ACL
	 * @param array $roles
	 * @return self
	 */
	public function setRoleConfig(array $roles) {
		foreach($roles as $role) {
			if(isset($role['parent']) && !$this->acl->hasRole($role['parent'])) {
				$this->acl->addRole($role['parent']);
			}
			if(!$this->acl->hasRole($role['name'])) {
				$this->acl->addRole($role['name'], $role['parent']);
			}
		}
		return $this;
	}
	
	public function setRouteConfig(array $routes) {
		foreach($routes as $data) {
			$resource = RouteGuard::routeNameToResource($data['route']);
			if(!$this->acl->hasResource($resource)) {
				$this->acl->addResource($resource);
			}
			if(isset($data['children'])) {
				foreach($data['children'] as $child) {
					$child = RouteGuard::routeNameToResource($child);
					if(!$this->acl->hasResource($child)) {
						$this->acl->addResource($child, $resource);
					}
				}
			}
			foreach($data['roles'] as $role) {
				$this->acl->allow($role, $resource);
			}
		}
	}
	
	public function isAllowed($resource, $priv = NULL) {
		$role = $this->getCurrentRoleId();
		//var_dump($role);
		/**
		 * When a resource does not exist, behaviour is currently to return false
		 * This could be adapted with a config switch to allow undefined resources
		 * to be potentially allowed, though inherently unsafe
		 */
		if(!$this->acl->hasResource($resource)) {
			return false;
		}
		
		
		return $this->acl->isAllowed($role, $resource, $priv);
		var_dump($role);
		
		return false;
	}
	
	
}