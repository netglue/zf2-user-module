<?php
/**
 * A factory to create a password list instance from configuration
 *
 */
namespace NetglueUser\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use NetglueUser\PasswordList\DbWordList;
use NetglueUser\PasswordList\ArrayWordList;

class PasswordListFactory implements FactoryInterface {
	
	/**
	 * Return the password list instance configured
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return PasswordListInterface
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('config');
		$options = isset($config['netglue_user']['password_list_options']) ? $config['netglue_user']['password_list_options'] : NULL;
		
		$type = strtolower($options['name']);
		$options = $options['options'];
		if($type === 'db') {
			if(!isset($options['dbService'])) {
				throw new \RuntimeException('No db adapter service configured for the database driven password list');
			}
			$adapter = $serviceLocator->get($options['dbService']);
			$list = new DbWordList($adapter, $options['tableName'], $options['columnName']);
			return $list;
		}
		$words = array();
		if(isset($options['data']) && is_array($options['data'])) {
			$words = $options['data'];
		}
		$list = new ArrayWordList($words);
		return $list;
	}
	
}