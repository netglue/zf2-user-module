<?php

namespace NetglueUser\Service;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ControllerFactory implements AbstractFactoryInterface {
	
	protected $aliases = array();
	
	/**
	 * Determine if we can create a service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return bool
	 */
	public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
		if(array_key_exists($requestedName, $this->aliases)) {
			return true;
		}
		return (bool) preg_match('/^NetglueUser\\Controller/', $requestedName);
	}
	
	/**
	 * Create service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return mixed
	 */
	public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
		$sl = $serviceLocator->getServiceLocator();
		if(array_key_exists($requestedName, $this->aliases)) {
			$class = $this->aliases[$requestedName];
		} else {
			$class = $requestedName;
		}
		$controller = new $class;
		if(method_exists($controller, 'setUserService')) {
			$controller->setUserService($sl->get('NetglueUser\Service\User'));
		}
		
		
		return $controller;
	}
	
	public function setAliases(array $aliases) {
		$this->aliases = $aliases;
		return $this;
	}
	
}