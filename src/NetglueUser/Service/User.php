<?php

namespace NetglueUser\Service;

/**
 * Object Manager Aware
 */
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Service Locator Aware
 */
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Form Hydrator
 */
use Zend\Stdlib\Hydrator\ClassMethods as Hydrator;

/**
 * User Entity
 */
use NetglueUser\Entity\User as UserEntity;

/**
 * Event Manager Aware
 */
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;

class User implements 
	ObjectManagerAwareInterface,
	ServiceLocatorAwareInterface,
	EventManagerAwareInterface {
	
	
	const EVENT_NEW_USER = 'netglue_user.user.create';
	
	/**
	 * Doctrine Object Manager
	 * @var ObjectManager
	 */
	protected $objectManager;
	
	/**
	 * Service Locator
	 * @var ServiceLocatorInterface|NULL
	 */
	protected $serviceLocator;
	
	/**
	 * Event Manager
	 * @var EventManagerInterface|NULL
	 */
	protected $eventManager;
	
	/**
	 * Form Hydrator
	 * @var Hydrator
	 */
	protected $hydrator;
	
	/**
	 * Return a form for creating Users in an admin context
	 * @return Form
	 */
	public function getCreateUserForm() {
		$formManager = $this->serviceLocator->get('FormElementManager');
		$form = $formManager->get('NetglueUser\Form\NewUser');
		// Form needs complex validators
		$form->setInputFilter($this->getServiceLocator()->get('NetglueUser\InputFilter\NewUser'));
		// Hydrator
		$form->setHydrator($this->getFormHydrator());
		return $form;
	}
	
	/**
	 * Return form for editing users
	 * @return Form
	 */
	public function getEditUserForm() {
		$formManager = $this->serviceLocator->get('FormElementManager');
		$form = $formManager->get('NetglueUser\Form\EditUser');
		$form->setInputFilter($this->getServiceLocator()->get('NetglueUser\InputFilter\NewUser'));
		$form->setHydrator($this->getFormHydrator());
		return $form;
	}
	
	public function getLoginForm() {
		$form = new \NetglueUser\Form\Login;
		return $form;
	}
	
	/**
	 * Return a form for changing your own password
	 * @return \NetglueUser\Form\ChangePassword
	 */
	public function getChangePasswordForm() {
		$form = new \NetglueUser\Form\ChangePassword;
		$form->setInputFilter($this->getServiceLocator()->get('NetglueUser\InputFilter\ChangePassword'));
		return $form;
	}
	
	/**
	 * Update a user password using field values from the change password form provided
	 * @param \NetglueUser\Form\ChangePassword $form
	 * @return bool
	 */
	public function updatePassword(\NetglueUser\Form\ChangePassword $form) {
		$user = $this->getAuthenticationService()->getIdentity();
		if(!$user) {
			return false;
		}
		$data = $form->getData();
		// Verify existing password
		$crypt = $this->getPasswordHashingService();
		if(!$crypt->verify($data['oldPassword'], $user->getHashedPassword())) {
			$old = $form->get('oldPassword');
			$old->setMessages(array(
				'The password provided does not match the password we have on record for you',
			));
			return false;
		}
		$user->setHashedPassword($crypt->encrypt($data['password']));
		$this->getEventManager()->trigger(__FUNCTION__ .'.pre', $this, array(
			'user' => $user,
			'form' => $form,
		));
		$om = $this->getObjectManager();
		$om->persist($user);
		$om->flush();
		$this->getEventManager()->trigger(__FUNCTION__ .'.post', $this, array(
			'user' => $user,
			'form' => $form,
		));
		return true;
	}
	
	/**
	 * Return the authentication service from the service locator
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthenticationService() {
		return $this->getServiceLocator()->get('NetglueUser\Authentication\Service');
	}
	
	/**
	 * Save a new user from an administration context
	 * @param array $data
	 * @return UserEntity|false Either the new User or false on failure, i.e. validation error etc
	 */
	public function createUser(array $data) {
		$user = new UserEntity;
		$form = $this->getCreateUserForm();
		$hydrator = $this->getFormHydrator();
		$form->setHydrator($hydrator);
		$form->bind($user);
		$form->setData($data);
		if(!$form->isValid()) {
			return false;
		}
		$user = $form->getData();
		$hasher = $this->getPasswordHashingService();
		$user->setHashedPassword($hasher->encrypt($form->get('password')->getValue()));
		$this->getEventManager()->trigger(__FUNCTION__ .'.pre', $this, array(
			'user' => $user,
			'form' => $form,
		));
		$om = $this->getObjectManager();
		$om->persist($user);
		$om->flush();
		$this->getEventManager()->trigger(__FUNCTION__ .'.post', $this, array(
			'user' => $user,
			'form' => $form,
		));
		return $user;
	}
	
	/**
	 * Save an existing user from an admin context
	 * @param UserEntity $user
	 * @param array $data
	 * @return bool Whether the update was successful or not
	 */
	public function updateUser(UserEntity $user, array $data) {
		$form = $this->getEditUserForm();
		$form->bind($user);
		$form->setData($data);
		if(!$form->isValid()) {
			return false;
		}
		$pass = $form->get('password')->getValue();
		if(!empty($pass)) {
			$hasher = $this->getPasswordHashingService();
			$user->setHashedPassword($hasher->encrypt($form->get('password')->getValue()));
		}
		$this->getEventManager()->trigger(__FUNCTION__ .'.pre', $this, array(
			'user' => $user,
			'form' => $form,
		));
		$om = $this->getObjectManager();
		$om->persist($user);
		$om->flush();
		$this->getEventManager()->trigger(__FUNCTION__ .'.post', $this, array(
			'user' => $user,
			'form' => $form,
		));
		return true;
	}
	
	/**
	 * Return User Options
	 * @return \NetglueUser\Options\UserOptions
	 */
	public function getUserOptions() {
		return $this->getServiceLocator()->get('NetglueUser\Options\UserOptions');
	}
	
	/**
	 * Set object manager
	 * @param ObjectManager $objectManager
	 * @return User $this
	 */
	public function setObjectManager(ObjectManager $objectManager) {
		$this->objectManager = $objectManager;
		return $this;
	}
	
	/**
	 * Return ObjectManager 
	 * @return ObjectManager|NULL
	 */
	public function getObjectManager() {
		return $this->objectManager;
	}
	
	/**
	 * Set Service Locator
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return User $this
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
		return $this;
	}
	
	/**
	 * Return service locator
	 * @return ServiceLocatorInterface|NULL
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}
	
	/**
	 * Lazy load form hydrator
	 * @return Hydrator
	 */
	public function getFormHydrator() {
		if(!$this->hydrator instanceof Hydrator) {
			$this->hydrator = new Hydrator;
		}
		return $this->hydrator;
	}
	
	/**
	 * Return password hashing service
	 * @return PasswordCryptServiceInterface
	 */
	public function getPasswordHashingService() {
		return $this->getServiceLocator()->get('NetglueUser\Service\PasswordCrypt');
	}
	
	/**
	 * Set the event manager
	 * @param EventManagerInterface $events
	 * @return EventManagerAwareInterface $this
	 */
	public function setEventManager(EventManagerInterface $eventManager) {
		$identifiers = array(__CLASS__, get_called_class());
		$eventManager->setIdentifiers($identifiers);
		$this->eventManager = $eventManager;
		return $this;
	}
	
	/**
	 * Return an event manager instance
	 * @return EventManagerInterface
	 */
	public function getEventManager() {
		if(!$this->eventManager) {
			$this->setEventManager(new EventManager);
		}
		return $this->eventManager;
	}
	
	public function getUserRepository() {
		return $this->getServiceLocator()->get('NetglueUser\Repository\UserRepository');
	}
}