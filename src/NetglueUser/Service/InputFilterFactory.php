<?php
/**
 * Abstract factory for creating input filters configured with all the dependencies they might require
 */


namespace NetglueUser\Service;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class InputFilterFactory implements AbstractFactoryInterface {
	
	/**
	 * Determine if we can create a service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return bool
	 */
	public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
		return (bool) preg_match('/^NetglueUser\\\InputFilter/', $requestedName);
	}
	
	/**
	 * Create service with name
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param $name
	 * @param $requestedName
	 * @return mixed
	 */
	public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName) {
		$class = $requestedName;
		$filter = new $class;
		if(method_exists($filter, 'setPasswordValidator')) {
			$filter->setPasswordValidator($serviceLocator->get('NetglueUser\Validator\Password'));
		}
		if(method_exists($filter, 'setUsernameValidator')) {
			$filter->setUsernameValidator($serviceLocator->get('NetglueUser\Validator\Username'));
		}
		if(method_exists($filter, 'setNewUsernameValidator')) {
			$filter->setNewUsernameValidator($serviceLocator->get('NetglueUser\Validator\NewUsername'));
		}
		if(method_exists($filter, 'setNewEmailValidator')) {
			$filter->setNewEmailValidator($serviceLocator->get('NetglueUser\Validator\NewEmail'));
		}
		if(method_exists($filter, 'setUserOptions')) {
			$filter->setUserOptions($serviceLocator->get('NetglueUser\Options\UserOptions'));
		}
		$filter->init();
		return $filter;
	}
	
}