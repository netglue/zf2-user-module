<?php

namespace NetglueUser\Service\PasswordCrypt;

use NetglueUser\Service\PasswordCryptServiceInterface;


class Sha1 implements PasswordCryptServiceInterface {
	
	/**
	 * Salt for encrypting strings
	 * @var string|NULL
	 */
	protected $salt;
	
	/**
	 * Encrypt a string
	 * @param string $string
	 * @return string
	 */
	public function encrypt($string) {
		return sha1($string.$this->salt);
	}
	
	/**
	 * Set Salt
	 * @param string $salt
	 * @return Md5 $this
	 */
	public function setSalt($salt) {
		$this->salt = $salt;
		return $this;
	}
	
	/**
	 * Return salt
	 * @return string|NULL
	 */
	public function getSalt() {
		return $this->salt;
	}
	
	/**
	 * Verify a clear text string against an encrypted string
	 * @param string $clearText
	 * @param string $encrypted
	 * @return bool
	 */
	public function verify($clearText, $encrypted) {
		return $this->encrypt($clearText) === $encrypted;
	}
	
	
}