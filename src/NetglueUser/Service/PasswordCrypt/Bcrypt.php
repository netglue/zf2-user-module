<?php

namespace NetglueUser\Service\PasswordCrypt;

use NetglueUser\Service\PasswordCryptServiceInterface;

use Zend\Crypt\Password\Bcrypt as ZendBcrypt;

class Bcrypt implements PasswordCryptServiceInterface {
	
	/**
	 * Bcrypt instance
	 * @var ZendBcrypt
	 */
	protected $bcrypt;
	
	/**
	 * Constructor
	 * @return void
	 */
	public function __construct() {
		$this->bcrypt = new ZendBcrypt;
	}
	
	/**
	 * Return bcrypt instance
	 * @return ZendBcrypt
	 */
	public function getBcrypt() {
		return $this->bcrypt;
	}
	
	/**
	 * Set password cost
	 * @param int between 4 and 31
	 * @return Bcrypt $this
	 */
	public function setCost($cost) {
		$this->bcrypt->setCost($cost);
		return $this;
	}
	
	/**
	 * Return current password cost
	 * @return int
	 */
	public function getCost() {
		return $this->bcrypt->getCost();
	}
	
	/**
	 * Encrypt a string
	 * @param string $string
	 * @return string
	 */
	public function encrypt($string) {
		return $this->bcrypt->create($string);
	}
	
	/**
	 * Verify a clear text string against an encrypted string
	 * @param string $clearText
	 * @param string $encrypted
	 * @return bool
	 */
	public function verify($clearText, $encrypted) {
		return $this->bcrypt->verify($clearText, $encrypted);
	}
	
	
}