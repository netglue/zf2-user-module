<?php
/**
 * Factory to return the configured password encryption service
 */

namespace NetglueUser\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class PasswordCryptServiceFactory implements FactoryInterface {
	
	/**
	 * Return the password hashing service
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return PasswordCryptServiceInterface
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('config');
		$options = isset($config['netglue_user']['password_options']) ? $config['netglue_user']['password_options'] : array();
		
		$class = isset($options['class']) ? strtolower($options['class']) : 'bcrypt';
		$salt = isset($options['salt']) ? $options['salt'] : NULL;
		$cost = isset($options['cost']) ? (int) $options['cost'] : 14;
		
		switch($class) {
			default:
			case 'bcrypt':
				$hash = new PasswordCrypt\Bcrypt;
				$hash->setCost($cost);
				break;
			case 'md5':
			case 'sha1':
				$c = '\NetglueUser\Service\PasswordCrypt\\'.ucfirst($class);
				$hash = new $c;
				$hash->setSalt($salt);
				break;
		}
		return $hash;
	}
	
}