<?php
/**
 * Route 'Guard' object that is configured with an array of route names that represent resources for an ACL
 */


namespace NetglueUser\Guard;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;

use NetglueUser\Session\RedirectContainer;

class Route implements ListenerAggregateInterface {

	/**
	 * Error Identifier set in the MvcEvent when the current route is not authorized
	 */
	const ERROR = 'error-unauthorised-route';

	/**
	 * Listeners
	 * @var array
	 * @see \Zend\EventManager\ListenerAggregateInterface
	 */
	protected $listeners = array();

	protected $aclService;

	protected $session;

	public function __construct($aclService) {
		$this->aclService = $aclService;
	}

	/**
	 * Attach to the Route event
	 * @param EventManagerInterface $events
	 * @return void
	 */
	public function attach(EventManagerInterface $events) {
		$this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'), -1000);
	}

	/**
	 * Detach registered listeners with the given event manager
	 * @param EventManagerInterface $events
	 * @return void
	 */
	public function detach(EventManagerInterface $events) {
		foreach($this->listeners as $index => $listener) {
			if($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}

	/**
	 * Helper method to turn a route name into a resource name
	 * @param string $routeName
	 * @return string
	 */
	public static function routeNameToResource($routeName) {
		return 'route/'.$routeName;
	}

	/**
	 * This is hacky.
	 */
	public function getSession() {
		if(!$this->session instanceof RedirectContainer) {
			$this->session = new RedirectContainer;
		}
		return $this->session;
	}

	/**
	 * Callback that listens to the route event
	 * @param MvcEvent $event
	 * @return void
	 */
	public function onRoute(MvcEvent $event) {
		// Retreive Current Route Name
		$match = $event->getRouteMatch();
		$routeName = $match->getMatchedRouteName();

		// Use the acl service to determine whether the current user is allowed access to
		// the current route
		$resource = $this->routeNameToResource($routeName);
		if($this->aclService->isAllowed($resource)) {
			return;
		}

		/**
		 * ACL Says nope...
		 */

		/**
		 * Store the request URI in the session so it's possible to redirect post login
		 */

		/**
		 * Either store the route, or the URI - They are mutually exclusive:
		 * URI is the best option because it will allow a redirect to the exact requested resource
		 * Rather than redirecting to a route name that may require additional parameters in
		 * order to be assembled
		 */

		$this->getSession()->setRedirectUri($event->getRequest()->getUri());
		//$this->getSession()->setRedirectRoute($routeName);

		// Set some useful params in the event and trigger a dispatch error
		$event->setError(static::ERROR);
		$event->setParam('route', $routeName);

		$app = $event->getTarget();
		$app->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $event);
	}

}
