<?php

namespace NetglueUser\Session;

use Zend\Session\Container;
use Zend\Session\ManagerInterface as Manager;

class RedirectContainer extends Container {
	
	const SESSION_NAME = 'NetglueUser_Session_AuthRedirect';
	
	
	public function __construct($name = 'Default', Manager $manager = null) {
		return parent::__construct(static::SESSION_NAME, $manager);
	}
	
	public function setRedirectUri($uri) {
		$this->redirectUri = $uri;
		unset($this->redirectRoute);
		return $this;
	}
	
	public function getRedirectUri() {
		return $this->redirectUri;
	}
	
	public function setRedirectRoute($route) {
		$this->redirectRoute = $route;
		unset($this->redirectUri);
		return $this;
	}
	
	public function getRedirectRoute() {
		return $this->redirectRoute;
	}
	
	public function isRoute() {
		$route = $this->getRedirectRoute();
		return !empty($route);
	}
	
	public function isUri() {
		$uri = $this->getRedirectUri();
		return !empty($uri);
	}
	
	public function hasRedirect() {
		return ($this->isUri() || $this->isRoute());
	}
	
	public function clear() {
		unset($this->redirectUri, $this->redirectRoute);
		return $this;
	}
	
}