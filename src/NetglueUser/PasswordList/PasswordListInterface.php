<?php
/**
 * An interface that exposes a single method to determine whether a string is contained in a list
 *
 */

namespace NetglueUser\PasswordList;

interface PasswordListInterface {
	
	/**
	 * Whether the given password exists in the list
	 * @param string $password
	 * @return bool
	 */
	public function exists($password);
	
}