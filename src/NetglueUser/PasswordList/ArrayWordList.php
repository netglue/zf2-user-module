<?php

namespace NetglueUser\PasswordList;
use ArrayObject;

class ArrayWordList extends ArrayObject implements PasswordListInterface {
	
	public function exists($password) {
		return in_array($password, $this->getArrayCopy());
	}
	
}
