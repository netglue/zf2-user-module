<?php

namespace NetglueUser\PasswordList;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DbWordList implements PasswordListInterface, AdapterAwareInterface {
	
	/**
	 * Db adapter
	 * @var Adapter|NULL
	 */
	protected $db;
	
	/**
	 * Table Name
	 * @var string|NULL
	 */
	protected $table;
	
	/**
	 * Column Name
	 * @var string|NULL
	 */
	protected $column;
	
	/**
	 * Constructor
	 * @param Adapter $db
	 * @param string $tableName
	 * @param string $columnName
	 * @return void
	 */
	public function __construct(Adapter $db = NULL, $tableName = NULL, $columnName = NULL) {
		if($db) {
			$this->setDbAdapter($db);
		}
		if($tableName) {
			$this->setTableName($tableName);
		}
		if($columnName) {
			$this->setColumnName($columnName);
		}
	}
	
	/**
	 * Set db adapter
	 *
	 * @param Adapter $adapter
	 * @return AdapterAwareInterface
	 */
	public function setDbAdapter(Adapter $db) {
		$this->db = $db;
		return $this;
	}
	
	/**
	 * Set Table Name
	 * @param string $table
	 * @return DbWordList $this
	 */
	public function setTableName($table) {
		$this->table = $table;
		return $this;
	}
	
	/**
	 * Set Column Name
	 * @param string $column
	 * @return DbWordList $this
	 */
	public function setColumnName($column) {
		$this->column = $column;
		return $this;
	}
	
	/**
	 * Whether the given string exists in the word list
	 * @param string $password
	 * @return bool
	 */
	public function exists($password) {
		if(empty($password)) {
			return false;
		}
		if(!$this->db) {
			throw new \RuntimeException('No database adapter has been configured');
		}
		$db = $this->db;
		if(NULL === $this->table) {
			throw new \RuntimeException('No table name has been provided');
		}
		if(NULL === $this->column) {
			throw new \RuntimeException('No column name has been provided');
		}
		$sql = new Sql($db);
		$select = $sql->select();
		$select->from(array('t' => $this->table));
		$select->columns(array('num' => new Expression("COUNT({$this->column})")));
		$select->where(array($this->column => $password));
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		$data = $results->current();
		$count = (int) $data['num'];
		return $count >= 1;
	}
	
	/**
	 * Given the path of text file containing a password on each line. Add passwords that don't exist already
	 * @param string $file
	 * @param int &$total Total entries in file
	 * @param int &$inserted Number of passwords inserted
	 * @return void
	 */
	public function populateFromTextFile($file, &$total, &$inserted) {
		if(!file_exists($file) || !is_readable($file)) {
			throw new \RuntimeException("{$file} either does not exist or is not readable");
		}
		$data = file($file);
		$inserted = 0;
		$total = 0;
		foreach($data as &$pass) {
			set_time_limit(0);
			$pass = trim($pass, "\r\n");
			if(empty($pass)) {
				continue;
			}
			$total++;
			if($this->addPassword($pass)) {
				$inserted++;
			}
		}
	}
	
	/**
	 * Enter the given string as a row in the table
	 * Catches exceptions thrown by the adapter and returns false, ie. for duplicate key entry
	 * @param string $pass
	 * @return bool
	 */
	protected function addPassword($pass) {
		if(empty($pass)) {
			return false;
		}
		if(!$this->db) {
			throw new \RuntimeException('No database adapter has been configured');
		}
		$db = $this->db;
		if(NULL === $this->table) {
			throw new \RuntimeException('No table name has been provided');
		}
		if(NULL === $this->column) {
			throw new \RuntimeException('No column name has been provided');
		}
		$sql = new Sql($db);
		$insert = $sql->insert($this->table);
		$insert->columns(array($this->column));
		$insert->values(array(
			$this->column => $pass,
		));
		$statement = $sql->prepareStatementForSqlObject($insert);
		try {
			$results = $statement->execute();
			return true;
		} catch(\Zend\Db\Adapter\Exception\InvalidQueryException $e) {
			return false;
		}
	}
	
}