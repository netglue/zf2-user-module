<?php
namespace NetglueUser\Permissions\Exception;

class UnauthorizedException extends \BadMethodCallException {
	
	const ERROR = 'unauthorized-exception';
	
}
