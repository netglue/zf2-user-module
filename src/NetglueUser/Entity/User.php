<?php

namespace NetglueUser\Entity;

use DateTime;
use NetglueUser\Model\User\UserInterface;
     

class User implements UserInterface {
	
	/**
	 * Id
	 * @var int|NULL
	 */
	protected $id;
	
	/**
	 * Username
	 * @var string|NULL
	 */
	protected $username;
	
	/**
	 * Email Address
	 * @var string|NULL
	 */
	protected $email;
	
	/**
	 * Hashed Password
	 * @var string|NULL
	 */
	protected $hashedPassword;
	
	/**
	 * Whether the account is active or not
	 * @var bool
	 */
	protected $active;
	
	/**
	 * Insertion time
	 * @var int Unix Timestamp
	 */
	protected $entryTime;
	
	/**
	 * Date Created
	 * @var DateTime|NULL
	 */
	protected $dateCreated;
	
	/**
	 * ACL Role
	 * @var string
	 */
	protected $role;
	
	public function __construct() {
		$this->setEntryTime(time());
	}
	
	public function toJsonArray() {
		return array(
			'id' => $this->id,
			'username' => $this->username,
			'email' => $this->email,
			'active' => $this->isActive(),
			'entryTime' => $this->entryTime * 1000,
		);
	}
	
	/**
	 * Return a unique identifier for the user
	 * @return int|NULL
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Return the username
	 * @return string|NULL
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * Set the username
	 * @param string $username
	 * @return UserInterface $this
	 */
	public function setUsername($username) {
		$username = empty($username) ? NULL : $username;
		$this->username = $username;
		return $this;
	}
	
	/**
	 * Set hashed password
	 * @param string $password
	 * @return UserInterface $this
	 */
	public function setHashedPassword($password) {
		$this->hashedPassword = $password;
		return $this;
	}
	
	/**
	 * Return a hash of the current password
	 * @return string|NULL
	 */
	public function getHashedPassword() {
		return $this->hashedPassword;
	}
	
	/**
	 * Deactivate the user account
	 * @return bool Whether deactivation was successful or not
	 */
	public function deactivate() {
		$this->active = false;
		return true;
	}
	
	/**
	 * Activate the user account
	 * @return bool
	 */
	public function activate() {
		$this->active = true;
		return $this;
	}
	
	/**
	 * Whether the account is active or not
	 * @return bool
	 */
	public function isActive() {
		return true === $this->active;
	}
	
	/**
	 * Set the email address
	 * @param string $email
	 * @return UserInterface
	 */
	public function setEmail($email) {
		$this->email = trim(strtolower($email));
		$this->email = empty($this->email) ? NULL : $this->email;
		return $this;
	}
	
	/**
	 * Return email address
	 * @return string|NULL
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * Set the entry time, creation time with a timestamp
	 * @param int $time
	 * @return User $this
	 */
	public function setEntryTime($time) {
		$this->entryTime = (int) $time;
		return $this;
	}
	
	/**
	 * Return entry time
	 * @return int
	 */
	public function getEntryTime() {
		return $this->entryTime;
	}
	
	/**
	 * Set 'active' flag
	 * @param bool $flag
	 * @return User $this
	 */
	public function setActive($flag) {
		$this->active = (bool) $flag;
		return $this;
	}
	
	/**
	 * Get value of the active flag
	 * @return bool
	 */
	public function getActive() {
		return $this->active;
	}
	
	/**
	 * Return the creation date
	 * @return DateTime
	 */
	public function getDateCreated() {
		if(!$this->dateCreated) {
			$this->dateCreated = new DateTime;
		}
		$this->dateCreated->setTimestamp($this->getEntryTime());
		return $this->dateCreated;
	}
	
	/**
	 * Set the creation date
	 * @param DateTime $date
	 * @return User $this
	 */
	public function setDateCreated(DateTime $date) {
		$this->dateCreated = $date;
		return $this->setEntryTime($date->getTimestamp());
	}
	
	public function getRole() {
		return $this->role;
	}
	
	public function setRole($role) {
		$role = empty($role) ? NULL : $role;
		$this->role = $role;
		return $this;
	}
}