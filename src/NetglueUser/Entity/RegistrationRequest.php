<?php

namespace NetglueUser\Entity;

class RegistrationRequest {
	
	protected $id;
	
	public function setId($id) {
		$this->id = (int) $id;
		return $this;
	}
	
	public function getId() {
		return $this->id;
	}
	
}