<?php
namespace NetglueUser\Repository;

use Doctrine\ORM\EntityRepository;

use NetglueUser\Entity\User as UserEntity;

class UserRepository extends EntityRepository {
	
	protected $class = 'NetglueUser\Entity\User';
	
	/**
	 * Whether the given email address is present in the database as an email address only
	 * @param string $email
	 * @return bool
	 */
	public function emailAddressExists($email) {
		$q = $this->_em->createQuery("SELECT COUNT(u.email) FROM {$this->class} u WHERE u.email = :email");
		$q->setParameter('email', $email);
		$count = (int) $q->getSingleScalarResult();
		return $count >= 1;
	}
	
	/**
	 * Whether the given string is present in the database as an email address or a username
	 * @param string $emailOrUsername
	 * @return bool
	 */
	public function identityExists($emailOrUsername) {
		$q = $this->_em->createQuery("SELECT COUNT(u.email) FROM {$this->class} u WHERE u.email = :email OR u.username = :email");
		$q->setParameter('email', $emailOrUsername);
		$count = (int) $q->getSingleScalarResult();
		return $count >= 1;
	}
	
	/**
	 * Whether the given username is present in the database as a username address only
	 * @param string $username
	 * @return bool
	 */
	public function usernameExists($username) {
		$q = $this->_em->createQuery("SELECT COUNT(u.username) FROM {$this->class} u WHERE u.username = :username");
		$q->setParameter('username', $username);
		$count = (int) $q->getSingleScalarResult();
		return $count >= 1;
	}
	
	/**
	 * Whether the username or email address exists for anyone other than the given User
	 * @param string $emailOrUsername
	 * @param UserEntity $context
	 * @return bool True if no other identity has the given username/email other than the given user, or false if there are any other accounts using the provided $emailOrUsername
	 */
	public function checkIdentity($emailOrUsername, UserEntity $user) {
		$q = $this->_em->createQuery("
			SELECT COUNT(u.id) FROM {$this->class} u WHERE
			(u.email = :email OR u.username = :email) AND u.id != :id
		");
		$q->setParameter('email', $emailOrUsername);
		$q->setParameter('id', $user->getId());
		$count = (int) $q->getSingleScalarResult();
		return $count === 0;
	}
	
	/**
	 * Return user accounts where the email address or username matches the given string
	 * @param string $query
	 * @return array
	 */
	public function searchIdentity($query, $limit = 50) {
		$query = '%'.trim($query, '%').'%';
		$q = $this->_em->createQuery("SELECT u FROM {$this->class} u WHERE u.username LIKE :query OR u.email LIKE :query ORDER BY u.username ASC, u.email ASC");
		$q->setParameter('query', $query);
		$q->setMaxResults($limit);
		return $q->getResult();
	}
	
	/**
	 * Locate a single user by id
	 * @param int|string $id
	 * @return UserEntity|false
	 */
	public function findById($id) {
		$id = (int) $id;
		$users = parent::findById($id);
		if(count($users) === 1) {
			return current($users);
		}
		return false;
	}
	
	/**
	 * Locate a user by either the username or email address
	 * @param string $emailOrUsername
	 * @returnUserEntity|false
	 */
	public function findByIdentity($emailOrUsername) {
		$q = $this->_em->createQuery("SELECT u FROM {$this->class} u WHERE u.username = :id OR u.email = :id");
		$q->setParameter('id', $emailOrUsername);
		return $q->getResult();
	}
	
}