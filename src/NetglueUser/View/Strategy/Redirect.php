<?php

namespace NetglueUser\View\Strategy;

use Zend\Mvc\MvcEvent;

use Zend\Http\Response;

class Redirect extends AbstractStrategy {
	
	protected $redirectUri;
	
	protected $redirectRoute;
	
	public function setRedirectUri($uri) {
		$this->redirectUri = $uri;
		return $this;
	}
	
	public function getRedirectUri() {
		return $this->redirectUri;
	}
	
	public function setRedirectRoute($route) {
		$this->redirectRoute = $route;
		return $this;
	}
	
	public function getRedirectRoute() {
		return $this->redirectRoute;
	}
	
	public function onDispatchError(MvcEvent $event) {
		// Do nothing if the result is a response object
		$result   = $event->getResult();
		$response = $event->getResponse();
		
		if($result instanceof Response || ($response && ! $response instanceof Response)) {
			return;
		}
		
		$match = $event->getRouteMatch();
		if(!$match) {
			// No route was matched? Perhaps Error triggerred in boostrapping?
			return;
		}
		
		if(!$this->isAclError($event)) {
			// Not an error condition we are supposed to handle...
			return;
		}
		
		$router = $event->getRouter();
		
		$uri = $this->redirectUri;
		if(NULL === $this->redirectUri) {
			if(NULL !== $this->redirectRoute) {
				$uri = $router->assemble(array(), array('name' => $this->redirectRoute));
			}
		}
		
		if(empty($uri)) {
			throw new \RuntimeException('No redirect uri, nor redirect route has been set for redirection strategy');
		}
		
		$response = $response ?: new Response();
		$response->getHeaders()->addHeaderLine('Location', $uri);
		$response->setStatusCode(302);
		$event->setResponse($response);
	}
	
}