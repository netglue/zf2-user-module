<?php

namespace NetglueUser\View\Strategy;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;

use NetglueUser\Permissions\Exception\UnauthorizedException;
use NetglueUser\Guard\Route as RouteGuard;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Application as MvcApplication;

abstract class AbstractStrategy extends AbstractListenerAggregate {
	
	public function attach(EventManagerInterface $events) {
		$this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), -5000);
	}
	
	abstract public function onDispatchError(MvcEvent $event);
	
	protected function isRouteGuardError(MvcEvent $event) {
		return $event->getError() === RouteGuard::ERROR;
	}
	
	protected function isUnathorizedException(MvcEvent $event) {
		$error = $event->getError();
		if($error === MvcApplication::ERROR_EXCEPTION) {
			$exception = $event->getParam('exception');
			if($exception instanceof UnauthorisedException) {
				return true;
			}
		}
		return false;
	}
	
	protected function isAclError(MvcEvent $event) {
		return $this->isRouteGuardError($event) || $this->isUnathorizedException($event);
	}
}