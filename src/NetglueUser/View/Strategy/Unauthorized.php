<?php

namespace NetglueUser\View\Strategy;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

use Zend\Http\Response as HttpResponse;
use Zend\Stdlib\ResponseInterface as Response;


class Unauthorized extends AbstractStrategy {
	
	protected $template = 'error/403';
	
	public function setTemplate($template) {
		$this->template = $template;
		return $this;
	}
	
	public function getTemplate() {
		return $this->template;
	}
	
	
	
	public function onDispatchError(MvcEvent $event) {
		// Do nothing if the result is a response object
		$result   = $event->getResult();
		$response = $event->getResponse();
		
		if($result instanceof Response || ($response && ! $response instanceof HttpResponse)) {
			return;
		}
		if(!$this->isAclError($event)) {
			return;
		}
		
		$view = new ViewModel;
		
		if($this->isRouteGuardError($event)) {
			/**
			 * Route Guard has triggered the error
			 */
			$view->error = $event->getParam('error');
			$view->route = $event->getParam('route');
			
		} elseif($this->isUnathorizedException($event)) {
			$exception = $event->getParam('exception');
			$view->reason = $exception->getMessage();
			$view->error = UnauthorisedException::ERROR;
		}
		
		// Set a Response (To stop further evaluation of error condition by other listeners?)
		$response = $response ?: new HttpResponse;
		$view->setTemplate($this->getTemplate());
		$event->getViewModel()->addChild($view);
		$response->setStatusCode(403);
		$event->setResponse($response);
	}
}