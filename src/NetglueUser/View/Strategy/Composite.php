<?php

namespace NetglueUser\View\Strategy;

use Zend\Mvc\MvcEvent;

use Zend\Http\Response;

use Zend\Authentication\AuthenticationService;

class Composite extends AbstractStrategy {
	
	protected $authService;
	
	protected $redirectStrategy;
	
	protected $unauthStrategy;
	
	
	public function setAuthenticationService(AuthenticationService $service) {
		$this->authService = $service;
		return $this;
	}
	
	public function setRedirectStrategy(Redirect $strategy) {
		$this->redirectStrategy = $strategy;
		return $this;
	}
	
	public function setUnauthStrategy(Unauthorized $strategy) {
		$this->unauthStrategy = $strategy;
		return $this;
	}
	
	public function onDispatchError(MvcEvent $event) {
		if(!$this->authService->hasIdentity()) {
			return $this->redirectStrategy->onDispatchError($event);
		}
		return $this->unauthStrategy->onDispatchError($event);
	}
}