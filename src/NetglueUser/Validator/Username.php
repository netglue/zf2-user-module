<?php
/**
 * Username Validator
 * 
 * Currently only checks string length and a simple pattern
 * but it's here to allow further configurable validation
 */
namespace NetglueUser\Validator;

use Zend\Validator\AbstractValidator;

class Username extends AbstractValidator {
	
	const INVALID = 'usernameNotString';
	const TOO_SHORT = 'usernameTooShort';
	const TOO_LONG = 'usernameTooLong';
	const FAIL_REGEX = 'usernameFailsRegex';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => 'Invalid value. String expected',
		self::TOO_SHORT => 'Usernames must be a minimum of %min% characters long',
		self::TOO_LONG => 'Usernames must be no greater than %max% characters long',
		self::FAIL_REGEX => 'The username provided contains characters that are not allowed',
	);
	
	/**
	 * Message variables
	 * @var array
	 */
	protected $messageVariables = array(
		'min' => array('options' => 'minLength'),
		'max' => array('options' => 'maxLength'),
	);
	
	/**
	 * Validation Options
	 * @var array
	 */
	protected $options = array(
		'encoding' => 'ascii',
		'minLength' => 8,
		'maxLength' => NULL,
		'pattern' => '/^[a-z0-9]+[a-z0-9@\._-]+$/i',
	);
	
	/**
	 * Return value of encoding option
	 * @return string
	 */
	public function getEncoding() {
		return $this->options['encoding'];
	}
	
	/**
	 * Set string encoding
	 * Hard coded to only accept ascii encoding
	 * @param string $encoding
	 * @return Username $this
	 */
	public function setEncoding($encoding) {
		if(strtolower($encoding) !== 'ascii') {
			$encoding = 'ascii';
		}
		$this->options['encoding'] = $encoding;
		return $this;
	}
	
	/**
	 * Set Username Regex
	 * @param string $pattern
	 * @return Username $this
	 */
	public function setPattern($pattern) {
		$this->options['pattern'] = $pattern;
		return $this;
	}
	
	/**
	 * Return username regex
	 * @return string
	 */
	public function getPattern() {
		return $this->options['pattern'];
	}
	
	/**
	 * Set min string length option
	 * @param int $min
	 * @return Username $this
	 */
	public function setMinLength($min) {
		if(!is_numeric($min)) {
			throw new \InvalidArgumentException('Min string length should be a number');
		}
		$max = $this->getMaxLength();
		if(!is_null($max) && ($min > $max)) {
			throw new \InvalidArgumentException('Min length cannot be greater than max length');
		}
		$this->options['minLength'] = (int) $min;
		return $this;
	}
	
	/**
	 * Return value of min string length option
	 * @return int
	 */
	public function getMinLength() {
		return $this->options['minLength'];
	}
	
	/**
	 * Set Max length
	 * @param int $max
	 * @return Username $this
	 */
	public function setMaxLength($max) {
		if(NULL === $max) {
			$this->options['maxLength'] = NULL;
			return $this;
		}
		if(!is_numeric($max)) {
			throw new \InvalidArgumentException('Max string length must be a number or NULL');
		}
		$min = $this->getMinLength();
		if($max < $min) {
			throw new \InvalidArgumentException('Max string length cannot be less than the minimum string length');
		}
		$this->options['maxLength'] = (int) $max;
		return $this;
	}
	
	/**
	 * Return value of max length option
	 * @return int|NULL
	 */
	public function getMaxLength() {
		return $this->options['maxLength'];
	}
	
	/**
	 * Validation
	 * @param string $value
	 * @return bool
	 */
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
			return false;
		}
		$this->setValue($value);
		$length = strlen($value);
		if($length < $this->getMinLength()) {
			$this->error(self::TOO_SHORT);
		} elseif(!is_null($this->getMaxLength()) && $length > $this->getMaxLength()) {
			$this->error(self::TOO_LONG);
		}
		$regex = $this->getPattern();
		if(!preg_match($regex, $value)) {
			$this->error(self::FAIL_REGEX);
		}
		
		return count($this->getMessages()) === 0;
	}
}