<?php

namespace NetglueUser\Validator;
class NewUsername extends NewEmail {
	
	const EXISTS = 'identityExists';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => 'Invalid value. String expected',
		self::EXISTS => 'There is already an account on the system with the username provided',
	);
	
}