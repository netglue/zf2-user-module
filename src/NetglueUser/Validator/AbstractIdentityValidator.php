<?php
/**
 * Base abstract validator for checking the validity of usernames and emails
 * when creating or updating user accounts
 */

namespace NetglueUser\Validator;
use Zend\Validator\AbstractValidator;
use Doctrine\ORM\EntityRepository;
use NetglueUser\Entity\User;

abstract class AbstractIdentityValidator extends AbstractValidator {
	
	/**
	 * Doctrine Repository for locating Users
	 * @var EntityRepository|NULL
	 */
	protected $repo;
	
	/**
	 * User Context for validity check
	 * @var User|NULL
	 */
	protected $userContext;
	
	/**
	 * Set Repository for locating User
	 * @param EntityRepository $repo
	 * @return AbstractIdentityValidator $this
	 */
	public function setUserRepository(EntityRepository $repo) {
		$this->repo = $repo;
		return $this;
	}
	
	/**
	 * Return User Repository
	 * @return EntityRepository|NULL
	 */
	public function getUserRepository() {
		return $this->repo;
	}
	
	/**
	 * Set the user context for the validation checks
	 * @param User $user
	 * @return AbstractIdentityValidator $this
	 */
	public function setUserContext(User $user) {
		$this->userContext = $user;
		return $this;
	}
	
	/**
	 * Return the User that defines the context for validity
	 * @return User|NULL
	 */
	public function getUserContext() {
		return $this->userContext;
	}
	
}