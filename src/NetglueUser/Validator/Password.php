<?php

namespace NetglueUser\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Stdlib\StringUtils;
use Zend\Stdlib\StringWrapper\StringWrapperInterface as StringWrapper;
use NetglueUser\PasswordList\PasswordListInterface as PasswordList;

class Password extends AbstractValidator {
	
	const REGEX_MIXED_CASE = '/(?U-msi)(.*[[:lower:]]+.*[[:upper:]]+)|(.*[[:upper:]]+.*[[:lower:]]+)/';
	const REGEX_NUMBERS = '/[0-9]/';
	const REGEX_PUNCT = '/[^a-zA-Z0-9]/';
	
	const INVALID = 'passwordNotString';
	const TOO_SHORT = 'passwordTooShort';
	const TOO_LONG = 'passwordTooLong';
	const NO_NUMBERS = 'passwordFailsNumberRegex';
	const NOT_MIXED_CASE = 'passwordFailsMixedCaseRegex';
	const NO_PUNCT = 'passwordFailsPunctuationRegex';
	const IN_WORDLIST = 'passwordPresentInWordList';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => 'Invalid value. String expected',
		self::TOO_SHORT => 'Passwords must be a minimum of %min% characters long',
		self::TOO_LONG => 'Passwords must be no greater than %max% characters long',
		self::NO_NUMBERS => 'Passwords must contain at least 1 number',
		self::NOT_MIXED_CASE => 'Passwords must contain a mixture of upper and lowercase letters',
		self::NO_PUNCT => 'Passwords must contain at least 1 character that is not a letter or a number',
		self::IN_WORDLIST => 'The password provided is in our list of disallowed passwords',
	);
	
	/**
	 * Message variables
	 * @var array
	 */
	protected $messageVariables = array(
		'min' => array('options' => 'minLength'),
		'max' => array('options' => 'maxLength'),
	);
	
	/**
	 * Validation Options
	 * @var array
	 */
	protected $options = array(
		'encoding' => 'UTF-8',
		'minLength' => 8,
		'maxLength' => NULL,
		'requireNumbers' => true,
		'requireMixedCase' => true,
		'requirePunctuation' => false,
		'useWordlist' => false,
		'wordList' => NULL,
	);
	
	/**
	 * String Wrapper
	 * @var StringWrapper|NULL
	 */
	protected $stringWrapper;
	
	/**
	 * Password List
	 * @var PasswordList|NULL
	 */
	protected $wordList;
	
	/**
	 * Return value of encoding option
	 * @return string
	 */
	public function getEncoding() {
		return $this->options['encoding'];
	}
	
	/**
	 * Set string encoding
	 * Also Resets the string wrapper with the new encoding
	 * @param string $encoding
	 * @return Password $this
	 */
	public function setEncoding($encoding) {
		$this->stringWrapper = StringUtils::getWrapper($encoding);
		$this->options['encoding'] = $encoding;
		return $this;
	}
	
	/**
	 * Set min string length option
	 * @param int $min
	 * @return Password $this
	 */
	public function setMinLength($min) {
		if(!is_numeric($min)) {
			throw new \InvalidArgumentException('Min string length should be a number');
		}
		$max = $this->getMaxLength();
		if(!is_null($max) && ($min > $max)) {
			throw new \InvalidArgumentException('Min length cannot be greater than max length');
		}
		$this->options['minLength'] = (int) $min;
		return $this;
	}
	
	/**
	 * Return value of min string length option
	 * @return int
	 */
	public function getMinLength() {
		return $this->options['minLength'];
	}
	
	/**
	 * Set Max length
	 * @param int $max
	 * @return Password $this
	 */
	public function setMaxLength($max) {
		if(NULL === $max) {
			$this->options['maxLength'] = NULL;
			return $this;
		}
		if(!is_numeric($max)) {
			throw new \InvalidArgumentException('Max string length must be a number or NULL');
		}
		$min = $this->getMinLength();
		if($max < $min) {
			throw new \InvalidArgumentException('Max string length cannot be less than the minimum string length');
		}
		$this->options['maxLength'] = (int) $max;
		return $this;
	}
	
	/**
	 * Return value of max length option
	 * @return int|NULL
	 */
	public function getMaxLength() {
		return $this->options['maxLength'];
	}
	
	/**
	 * Set the require numbers flag
	 * @param bool $flag
	 * @return Password $this
	 */
	public function setRequireNumbers($flag) {
		$this->options['requireNumbers'] = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether numbers are required
	 * @return bool
	 */
	public function requiresNumbers() {
		return $this->options['requireNumbers'];
	}
	
	/**
	 * Set the require mixed case letters flag
	 * @param bool $flag
	 * @return Password $this
	 */
	public function setRequireMixedCase($flag) {
		$this->options['requireMixedCase'] = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether mixed case letters are required
	 * @return bool
	 */
	public function requiresMixedCase() {
		return $this->options['requireMixedCase'];
	}
	
	/**
	 * Set the require punctuation flag
	 * @param bool $flag
	 * @return Password $this
	 */
	public function setRequirePunctuation($flag) {
		$this->options['requirePunctuation'] = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether punctuation is required
	 * @return bool
	 */
	public function requiresPunctuation() {
		return $this->options['requirePunctuation'];
	}
	
	/**
	 * Return string wrapper for correctly detecting string length in any encoding
	 * @return StringWrapper
	 */
	public function getStringWrapper() {
		if(!$this->stringWrapper) {
			$this->stringWrapper = StringUtils::getWrapper($this->getEncoding());
		}
		return $this->stringWrapper;
	}
	
	/**
	 * Set String Wrapper
	 * @param StringWrapper $stringWrapper
	 * @return Password $this
	 */
	public function setStringWrapper(StringWrapper $stringWrapper) {
		$this->stringWrapper = $stringWrapper;
		return $this;
	}
	
	/**
	 * Set the word list to use for comparing the password
	 * @param PasswordList $list
	 * @return Password $this
	 */
	public function setWordList(PasswordList $list) {
		$this->wordList = $list;
		return $this;
	}
	
	/**
	 * Return word list checker
	 * @return PasswordList|NULL
	 */
	public function getWordList() {
		return $this->wordList;
	}
	
	/**
	 * Enable usage of word lists
	 * @param bool $flag
	 * @return Password $this
	 */
	public function setUseWordList($flag) {
		$this->options['useWordList'] = (bool) $flag;
		return $this;
	}
	
	/**
	 * Whether word lists are enabled
	 * @return bool
	 */
	public function wordListEnabled() {
		return $this->options['useWordList'];
	}
	
	/**
	 * Validation
	 * @param string $value
	 * @return bool
	 */
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
			return false;
		}
		$this->setValue($value);
		$length = $this->getStringWrapper()->strlen($value);
		if($length < $this->getMinLength()) {
			$this->error(self::TOO_SHORT);
		} elseif(!is_null($this->getMaxLength()) && $length > $this->getMaxLength()) {
			$this->error(self::TOO_LONG);
		}
		
		if($this->requiresMixedCase() && !preg_match(self::REGEX_MIXED_CASE, $value)) {
			$this->error(self::NOT_MIXED_CASE);
		}
		
		if($this->requiresNumbers() && !preg_match(self::REGEX_NUMBERS, $value)) {
			$this->error(self::NO_NUMBERS);
		}
		
		if($this->requiresPunctuation() && !preg_match(self::REGEX_PUNCT, $value)) {
			$this->error(self::NO_PUNCT);
		}
		
		if($this->wordListEnabled() && $this->getWordList()) {
			if(true === $this->getWordList()->exists($value)) {
				$this->error(self::IN_WORDLIST);
			}
		}
		
		return count($this->getMessages()) === 0;
	}
}