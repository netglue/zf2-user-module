<?php

namespace NetglueUser\Validator;
use Zend\Validator\AbstractValidator;
class NewEmail extends AbstractIdentityValidator {
	
	const INVALID = 'notString';
	const EXISTS = 'identityExists';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => 'Invalid value. String expected',
		self::EXISTS => 'There is already an account on the system with the email address provided',
	);
	
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
		}
		$this->setValue($value);
		
		if($this->getUserContext()) {
			if($this->getUserRepository()->checkIdentity($value, $this->getUserContext()) === false) {
				$this->error(self::EXISTS);
			}
		} else {
			if($this->getUserRepository()->identityExists($value) === true) {
				$this->error(self::EXISTS);
			}
		}
		
		return count($this->getMessages()) === 0;
	}
	
}