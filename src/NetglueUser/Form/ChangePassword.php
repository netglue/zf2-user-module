<?php

namespace NetglueUser\Form;

use Zend\Form\Form as ZendForm;
use Zend\InputFilter\InputFilterProviderInterface;


class ChangePassword extends ZendForm {
	
	const FORM_NAME = 'netglue_change_password';
	
	public function __construct($name = NULL) {
		parent::__construct(self::FORM_NAME);
		
		$this->setAttribute('id', self::FORM_NAME);
		
		$this->add(array(
			'type' => 'password',
			'name' => 'oldPassword',
			'options' => array(
				'label' => 'Old Password',
			),
			'attributes' => array(
				'title' => 'Enter your current password so we can be sure it really is you!',
			),
		));
		
		$this->add(array(
			'type' => 'password',
			'name' => 'password',
			'options' => array(
				'label' => 'New Password',
			),
			'attributes' => array(
				'title' => 'Passwords are case sensitive and must adhere to our password strength requirements',
			),
		));
		
		$this->add(array(
			'type' => 'password',
			'name' => 'verify',
			'options' => array(
				'label' => 'Verify New Password',
			),
			'attributes' => array(
				'title' => 'Re-enter your new password again to make sure you haven\'t made any mistakes',
			),
		));
		
		$this->add(array(
			'type' => 'button',
			'name' => 'login',
			'options' => array(
				'label' => 'Change Password',
			),
			'attributes' => array(
				'type' => 'submit',
			),
		));
	}
	
}