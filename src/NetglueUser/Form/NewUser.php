<?php

namespace NetglueUser\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class NewUser extends Form  {
	
	const FORM_NAME = 'netglue_user';
	
	public function __construct($name = NULL) {
		parent::__construct(self::FORM_NAME);
		
		$this->setAttribute('id', self::FORM_NAME);
	}
	
	public function init() {
		$this->add(array(
			'type' => 'text',
			'name' => 'username',
			'options' => array(
				'label' => 'Username',
			),
			'attributes' => array(
				'placeholder' => 'Unique Username',
				'title' => 'You can leave the username field blank to use the email address',
			),
		));
		
		$this->add(array(
			'type' => 'email',
			'name' => 'email',
			'options' => array(
				'label' => 'Email Address',
			),
			'attributes' => array(
				'placeholder' => 'user@domain.com',
				'title' => 'A valid email address is always required',
			),
		));
		
		$this->add(array(
			'type' => 'password',
			'name' => 'password',
			'options' => array(
				'label' => 'Password',
			),
			'attributes' => array(
				'title' => 'Passwords are subject to minimum strength requirements',
			),
		));
		
		$this->add(array(
			'type' => 'NetglueUserRole',
			'name' => 'role',
			'options' => array(
				'label' => 'User Role',
			),
			'attributes' => array(
				'title' => 'Select the role this user will fulfill on this system',
			),
		));
		
		$this->add(array(
			'type' => 'checkbox',
			'name' => 'active',
			'options' => array(
				'label' => 'Activate Account?',
				'use_hidden_element' => true,
				'checked_value' => 1,
				'unchecked_value' => 0,
			),
		));
		
		$this->add(array(
			'type' => 'button',
			'name' => 'saveUser',
			'options' => array(
				'label' => 'Save User',
			),
			'attributes' => array(
				'type' => 'submit',
			),
		));
		
	}
}
