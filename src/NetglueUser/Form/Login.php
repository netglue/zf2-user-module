<?php

namespace NetglueUser\Form;

use Zend\Form\Form as ZendForm;
use Zend\InputFilter\InputFilterProviderInterface;


class Login extends ZendForm implements InputFilterProviderInterface {
	
	const FORM_NAME = 'netglue_login';
	
	public function __construct($name = NULL) {
		parent::__construct(self::FORM_NAME);
		
		$this->setAttribute('id', self::FORM_NAME);
		$this->add(array(
			'type' => 'text',
			'name' => 'identity',
			'options' => array(
				'label' => 'Email or Username',
			),
			'attributes' => array(
				'placeholder' => 'user@domain.com',
				'title' => 'Enter your email address or username',
			),
		));
		
		$this->add(array(
			'type' => 'password',
			'name' => 'password',
			'options' => array(
				'label' => 'Password',
			),
			'attributes' => array(
				'title' => 'Passwords are case sensitive',
			),
		));
		
		$this->add(array(
			'type' => 'button',
			'name' => 'login',
			'options' => array(
				'label' => 'Sign In',
			),
			'attributes' => array(
				'type' => 'submit',
			),
		));
	}
	
	public function getInputFilterSpecification() {
		return array(
			array(
				'name' => 'identity',
				'required' => true,
			),
			array(
				'name' => 'password',
				'required' => true,
			),
		);
	}
}