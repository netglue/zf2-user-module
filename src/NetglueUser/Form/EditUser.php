<?php

namespace NetglueUser\Form;

class EditUser extends NewUser  {
	
	const FORM_NAME = 'netglue_user';
	
	public function setObject($object) {
		parent::setObject($object);
		$filter = $this->getInputFilter();
		
		// Password is now not required - only update if entered
		$pass = $filter->get('password');
		$pass->setRequired(false);
		$this->get('password')->setAttribute('title', 'Only enter a new password to change the old one');
		
		
		// Username and Email Validators need to be aware of object context
		$inputs = array(
			$filter->get('username'),
			$filter->get('email'),
		);
		foreach($inputs as $input) {
			$chain = $input->getValidatorChain();
			foreach($chain->getValidators() as $spec) {
				if(isset($spec['instance']) && is_object($spec['instance'])) {
					$validator = $spec['instance'];
					if(method_exists($validator, 'setUserContext')) {
						$validator->setUserContext($object);
					}
				}
			}
		}
		return;
	}
	
}