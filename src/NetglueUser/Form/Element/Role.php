<?php

namespace NetglueUser\Form\Element;

use Zend\Form\Element\Select;

class Role extends Select {
	
	/**
	 * Set Roles
	 * @param array $roles
	 */
	public function setRoles($roles) {
		//$this->roles = $roles;
		$this->valueOptions = array();
		foreach($roles as $role) {
			$this->valueOptions[$role] = $role;
		}
		$this->setValueOptions($this->valueOptions);
	}
	
}