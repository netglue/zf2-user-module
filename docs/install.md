# Installation

This module uses Doctrine 2 exclusively for persistence of user entities, so
you'll need to add the Doctrine module and Doctrine ORM module before this one.
	
	//...
	'modules' => array(
		'DoctrineModule',
		'DoctrineORMModule',
	),
	//...

And in composer.json

	require: {
		"doctrine/doctrine-orm-module" : "0.*"
	}

Installation for the `NetglueUser` module is github/packagist usual flavour. Module name is `NetglueUser` and composer name is `"netglue/zf2-user-module": "dev-master"`.
Your minimum stability setting will need to be set to dev I expect.

