# Password Hashing

The module exposes a simple interface for creating one-way hashes of passwords `NetglueUser\Service\PasswordCryptServiceInterface`

This interface has 2 methods `(string) encrypt($string)` and `(bool) verify($clearText, $hash)`

The purpose of the interface is to provide a single instance for hashing and verifying passwords and by default, the service manager will return this via the factory with the name `NetglueUser\Service\PasswordCrypt` so all you have to do to replace the hashing implementation entirely is setup a factory with this name that returns an instance implementing the `NetglueUser\Service\PasswordCryptServiceInterface` interface

## Defaults

By default, a thin wrapper is provided around `Zend\Crypt\Password\Bcrypt` with a CPU cost of 14

## Sha1 & MD5

It's not recommended to use these and they are placed there more for experimentation than anything else. It's worth reading [php.net/manual/en/faq.passwords.php](http://www.php.net/manual/en/faq.passwords.php)

# Password Validation

The module contains a custom password validator that is hooked up with various forms. I decided to create a custom validator rather than a chain because when you use multiple regex validators, the messages get overriden when there are multiple failures. Additionally, customising and translating the error messages should be easier this way and more specific. The options should be straight forward and are documented in the config file with the exception of 'word lists' which might need a bit more explanation.

# Word Lists

There's an interface for a 'Word List' that exposes a single method `(bool) exists($password)` in `NetglueUser\PasswordList\PasswordListInteface`.

Two types of word list are shipped, Db and Array. Whichever is configured is accessible with the service named `NetglueUser\PasswordList` and without any configuration the factory will return an empty implementation that extends `ArrayObject` so you could easily populate this object in other ways if wished. Example config for the array object word list:

	return array(
		'netglue_user' => array(
			'password_list_options' => array(
				'name' => 'array',
				'options' => array(
					'data' => array(
						'some-crap-password',
						'password!',
						'foobar',
						// etc
					),
				),
			),
		),
	);

For my purposes, I use the db variant and set up a table full of bad passwords by using the _(Primitive!)_ import function from a text file that you'll find in the `DbWordList` class. Configuration of this is pretty trivial:

	return array(
		'netglue_user' => array(
			'password_list_options' => array(
				'name' => 'db',
				'options' => array(
					'dbService' => 'Name of Adapter Factory/Service',
					'tableName' => 'password_list_table_name',
					'columnName' => 'column_containing_passwords',
				),
			),
		),
	);
