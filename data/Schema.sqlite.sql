BEGIN TRANSACTION;

/**
 * User Table
 */
DROP TABLE "ng_user";
CREATE TABLE 'ng_user' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	'username' TEXT,
	'email' TEXT NOT NULL,
	'password' TEXT NOT NULL,
	'active' INTEGER NOT NULL DEFAULT 1,
	'entry_time' INTEGER NOT NULL
);
DROP INDEX "idx_ng_user_username";
DROP INDEX "idx_ng_user_email";
CREATE UNIQUE INDEX 'idx_ng_user_username' ON "ng_user" ("username");
CREATE UNIQUE INDEX 'idx_ng_user_email' ON "ng_user" ("email");

/**
 * Password List Table
 */
DROP TABLE "ng_user_password_list";
CREATE TABLE 'ng_user_password_list' (
	'password' TEXT PRIMARY KEY NOT NULL
);

/**
 * Registration Request Table
 */
DROP TABLE "ng_user_registration_request";
CREATE TABLE 'ng_user_registration_request' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	'request_time' INTEGER NOT NULL,
	'confirm_time' INTEGER DEFAULT NULL,
	'remote_address' TEXT DEFAULT NULL,
	'email' TEXT NOT NULL,
	'username' TEXT NOT NULL,
	'password' TEXT NOT NULL
);
DROP INDEX "idx_ng_reg_req_email";
CREATE INDEX 'idx_ng_reg_req_email' ON 'ng_user_registration_request' ('email');



COMMIT;